<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Emba Jeans</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body style="background: #dddddd; font-size: 14px; font-family: Arial; color: #333">
<br>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="700" style="border-collapse: collapse;border:1px solid #d8d8d8;background: white;" >
 	<tr style="border:1px solid #d8d8d8;border-bottom:10px solid #dddddd">
            <td width="750" colspan="2">&nbsp;&nbsp;&nbsp;<img width="200" src="http://dev.embajeans.store/assets/images/logo-small.png" style="margin-top: 10px;"></td>
        </tr>
        
        <tr>
            <td colspan="3">
                <br>
                &nbsp;&nbsp;&nbsp;<b style="font-size: 16px; color: #333">Tagihan Transaksi : <?php echo $info_text1;?></b>
		<br><br>
                &nbsp;&nbsp;&nbsp;<font style="color: #333">Silakan melakukan pembayaran untuk tagihan <?php echo $info_text1;?></font>
                <br><br>
                &nbsp;&nbsp;&nbsp;<font style="color: #333">Total Belanja:</font>
                <br>
                &nbsp;&nbsp;&nbsp;<b style="font-size: 16px; color: #333">Rp <?php echo number_format($info_text2);?></b>
                <br><br>
                &nbsp;&nbsp;&nbsp;<font style="color: #333">Metode Pembayaran:</font>
                <br>
                &nbsp;&nbsp;&nbsp;<b style="font-size: 16px; color: #333"><?php echo ucfirst($info_text3);?></b>
                <br><br>
                &nbsp;&nbsp;&nbsp;<font style="color: #333">Transfer dapat dilakukan ke nomor rekening:</font>
                <br>
                &nbsp;&nbsp;&nbsp;<b style="font-size: 16px; color: #333"><?php echo $info_text4;?></b>
                <br><br>
                &nbsp;&nbsp;&nbsp;<font style="color: #333">Batas waktu pembayaran Anda:</font>
                <br>
                &nbsp;&nbsp;&nbsp;<b style="font-size: 16px; color: #333"><?php echo $info_text5;?></b>
                <br><br>
                &nbsp;&nbsp;&nbsp;<font style="color: #333">Rincian pemesananmu dapat dilihat di halaman "transaksi saya".</font>
                <br><br>
            </td>
 	</tr>
 	
</table>
<i style="font-size: 11px">
    <center>Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.</center>
</i>
<br>
</body>
</html>