<!DOCTYPE html>
<html lang="en">
<head>
  <title>Download Aplikasi</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="text-center">
            <img src="https://silakon.pupr.probolinggokab.go.id/assets/img/logo-white.png" class="img-responsive" style="padding-left: 10px; padding-right: 10px; margin-top: 10px;">
        </div>
    </div>
    <form name="frm_data" id="frm_data" method="post" action="" style="margin-top: 20px;">
        <div class="row">
            <div class="col-sm-6 col-xs-6">
                <input type="submit" name="btn_download" id="btn_download" class="btn btn-primary btn-block" style="border-radius: 0px" value="Download Aplikasi">
            </div>
            <div class="col-sm-6 col-xs-6">
                <input type="submit" name="btn_panduan" id="btn_panduan" class="btn btn-success btn-block" style="border-radius: 0px" value="Download Panduan">
            </div>
        </div>
    </form>    
</div>

</body>
</html>
