<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Akses Library
============================================================== */
class Akses {
    public function get_akses($method) {
        $has_access = $this->route_app($method);
        return $has_access;
    }
    
    protected function route_app($method){
        // path => lokasi model
        // route => lokasi function di dalam model
        
        $response = array(
            array('kode' => 'login', 'model' => 'auth', 'path' => 'auth', 'route' => 'login'),
            array('kode' => 'update_data', 'model' => 'auth', 'path' => 'auth', 'route' => 'update_data'),
            array('kode' => 'insert_log', 'model' => 'auth', 'path' => 'auth', 'route' => 'insert_log'),
            array('kode' => 'check_version', 'model' => 'auth', 'path' => 'auth', 'route' => 'check_version'),
            array('kode' => 'download_panduan', 'model' => 'auth', 'path' => 'auth', 'route' => 'download_panduan'),
            
            
            
            array('kode' => 'laporan_keuangan', 'model' => 'keuangan', 'path' => 'keuangan', 'route' => 'laporan_keuangan'),
            array('kode' => 'laporan_keuangan_detail', 'model' => 'keuangan', 'path' => 'keuangan', 'route' => 'laporan_keuangan_detail'),
            
            array('kode' => 'inbox_data', 'model' => 'inbox', 'path' => 'inbox', 'route' => 'inbox_data'),
            array('kode' => 'inbox_detail', 'model' => 'inbox', 'path' => 'inbox', 'route' => 'inbox_detail'),
            
            array('kode' => 'proyek_terbaru', 'model' => 'proyek', 'path' => 'proyek', 'route' => 'proyek_terbaru'),
            array('kode' => 'proyek_detail', 'model' => 'proyek', 'path' => 'proyek', 'route' => 'proyek_detail'),
            array('kode' => 'proyek_aktif', 'model' => 'proyek', 'path' => 'proyek', 'route' => 'proyek_aktif'),
            
            array('kode' => 'pekerjaan_detail', 'model' => 'proyek', 'path' => 'proyek', 'route' => 'pekerjaan_detail'),
            array('kode' => 'pekerjaan_detail_add', 'model' => 'proyek', 'path' => 'proyek', 'route' => 'pekerjaan_detail_add'),
            array('kode' => 'pekerjaan_detail_edit', 'model' => 'proyek', 'path' => 'proyek', 'route' => 'pekerjaan_detail_edit'),
            
            array('kode' => 'pekerjaan_detail_sub', 'model' => 'proyek', 'path' => 'proyek', 'route' => 'pekerjaan_detail_sub'),
            array('kode' => 'pekerjaan_detail_sub_add', 'model' => 'proyek', 'path' => 'proyek', 'route' => 'pekerjaan_detail_sub_add'),
            array('kode' => 'pekerjaan_detail_sub_edit', 'model' => 'proyek', 'path' => 'proyek', 'route' => 'pekerjaan_detail_sub_edit'),
            
            array('kode' => 'aktifitas_detail', 'model' => 'aktifitas', 'path' => 'aktifitas', 'route' => 'aktifitas_detail'),
            array('kode' => 'aktifitas_detail_data', 'model' => 'aktifitas', 'path' => 'aktifitas', 'route' => 'aktifitas_detail_data'),
            array('kode' => 'aktifitas_detail_data_uraian', 'model' => 'aktifitas', 'path' => 'aktifitas', 'route' => 'aktifitas_detail_data_uraian'),
            array('kode' => 'aktifitas_detail_add', 'model' => 'aktifitas', 'path' => 'aktifitas', 'route' => 'aktifitas_detail_add'),
            array('kode' => 'aktifitas_detail_add_data', 'model' => 'aktifitas', 'path' => 'aktifitas', 'route' => 'aktifitas_detail_add_data'),
            array('kode' => 'aktifitas_detail_data_edit', 'model' => 'aktifitas', 'path' => 'aktifitas', 'route' => 'aktifitas_detail_data_edit'),
            array('kode' => 'aktifitas_detail_edit_data', 'model' => 'aktifitas', 'path' => 'aktifitas', 'route' => 'aktifitas_detail_edit_data'),
            array('kode' => 'aktifitas_detail_edit', 'model' => 'aktifitas', 'path' => 'aktifitas', 'route' => 'aktifitas_detail_edit'),
            
            array('kode' => 'aktifitas_detail_sub', 'model' => 'aktifitas', 'path' => 'aktifitas', 'route' => 'aktifitas_detail_sub'),
            array('kode' => 'aktifitas_detail_sub_edit', 'model' => 'aktifitas', 'path' => 'aktifitas', 'route' => 'aktifitas_detail_sub_edit'),
            array('kode' => 'aktifitas_detail_sub_edit_save', 'model' => 'aktifitas', 'path' => 'aktifitas', 'route' => 'aktifitas_detail_sub_edit_save'),
            array('kode' => 'aktifitas_detail_sub_add', 'model' => 'aktifitas', 'path' => 'aktifitas', 'route' => 'aktifitas_detail_sub_add'),
            array('kode' => 'aktifitas_detail_sub_add_save', 'model' => 'aktifitas', 'path' => 'aktifitas', 'route' => 'aktifitas_detail_sub_add_save'),
            
            array('kode' => 'aktifitas_detail_sub_status', 'model' => 'aktifitas', 'path' => 'aktifitas', 'route' => 'aktifitas_detail_sub_status'),
            array('kode' => 'aktifitas_detail_verifikasi', 'model' => 'aktifitas', 'path' => 'aktifitas', 'route' => 'aktifitas_detail_verifikasi'),
            
            array('kode' => 'aktifitas_detail_verifikasi_action', 'model' => 'aktifitas', 'path' => 'aktifitas', 'route' => 'aktifitas_detail_verifikasi_action'),
            
            array('kode' => 'berita', 'model' => 'berita', 'path' => 'berita', 'route' => 'berita_terbaru'),
            array('kode' => 'berita_detail', 'model' => 'berita', 'path' => 'berita', 'route' => 'berita_detail'),
            
        );
        return $this->searchArrayKeyVal("kode", $method, $response);
    }
    
    protected function searchArrayKeyVal($sKey, $id, $array) {
        foreach ($array as $key => $val) {
            if ($val[$sKey] == $id) {
                return $val['model']."#".$val['path']."#".$val['route'];
            }
        }
        return false;
    }
}