<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Berita model
============================================================== */
class Berita extends MY_Model {
    
    public function __construct() {
        parent::__construct();
    }
    public function berita_terbaru($param){
        $limit = $param->param->limit;
        $query = "SELECT * FROM berita order by created_at desc";
        if($limit != ""){
            $query = "SELECT * FROM berita order by created_at desc LIMIT $limit";
        }
        
        $result = $this->db_prod->query($query);
        if($result->num_rows() > 0){
            $no = 0;
            foreach($result->result() as $row){
                $tgl_update = $this->format_date_indo($row->update_at);
                if($tgl_update == "01 Januari 1970"){
                    $tgl_update = "";
                }
                        
                $resp[] = array(
                    "id" => $row->id_berita,
                    "judul" => $row->judul,
                    "deskripsi" => $row->deskripsi,
                    "tanggal_dibuat" => $this->format_date_indo($row->created_at),
                    "tanggal_update" => $tgl_update,
                );
                $no++;
            }
            
            $response = array(
                "jumlah" => $no,
                "detail" => $resp,
            );
            
            // create respon html
            $html = "";
            foreach($response['detail'] as $row){
                $html .= "<div class='col-md-8 col-xs-8' style='font-size: 13px;'>
                            <a href='#/berita_detail/".$row['id']."'>
                                <span style='font-size: 13px; font-weight:bold; color:#333;'>
                                    ".$row['judul']."
                                </span>
                            </a>
                        </div>
                        <div class='col-md-4 col-xs-4' style='font-size: 13px;'>
                            <a href='#/berita_detail/".$row['id']."'>
                                <span style='color:#333;' class='pull-right'>".$row['tanggal_dibuat']."</span>
                            </a>
                        </div>
                        <div class='col-md-12 col-xs-12' style='font-size: 13px; margin-bottom:15px;'>
                            <a href='#/berita_detail/".$row['id']."'>
                                <span class=text-deskripsi>".$row['deskripsi']."</span>
                            </a>
                        </div>
                        ";
            }
            
            return $this->response_sukses($response, $html);
        }else{
            return $this->response_gagal_html("02", "Tidak ada berita terbaru");die();
        }
    }
    
    public function berita_detail($param)
    {
        if(empty($param->param->id)){
            return $this->response_gagal("02", "ID berita tidak tersedia");die();
        }
        
        $query = "SELECT * FROM berita where id_berita = ?";
        $result = $this->db_prod->query($query, $param->param->id);
        if($result->num_rows() > 0){
            $row = $result->row();
            $tgl_update = $this->format_date_indo($row->update_at);
            if($tgl_update == "01 Januari 1970"){
                $tgl_update = "";
            }
            
            $resp = array(
                "id" => $row->id_berita,
                "judul" => $row->judul,
                "deskripsi" => $row->deskripsi,
                "tanggal_dibuat" => $this->format_date_indo($row->created_at),
                "tanggal_update" => $tgl_update,
            );
            return $this->response_sukses($resp);
        }else{
            return $this->response_gagal_html("02", "Tidak ada berita terbaru");
        }
    }
    
}