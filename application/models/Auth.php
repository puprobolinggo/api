<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Auth model
============================================================== */
class Auth extends MY_Model {
    
    public function __construct() {
        parent::__construct();
    }
    public function login($param){
        $data = array();
        if(empty($param->param->id)){
            return $this->response_gagal("02", "Akun pengguna tidak tersedia");die();
        }
        if(empty($param->param->password)){
            return $this->response_gagal("02", "Kata sandi tidak tersedia");die();
        }
        $result = $this->get_data($param->param->id, $param->param->password);
        if($result->num_rows() > 0){
            $result = $result->row();
            $data["userid"] = $result->id;
            $data["username"] = $result->username;
            $data["pin"] = $result->pin;
            $data["nama"] = ucwords(strtolower($result->nama));
            $data["direktur"] = ucwords(strtolower($result->direktur));
            $data["alamat"] = ucwords(strtolower($result->alamat));
            $data["kontraktor_pelaksana"] = ucwords(strtolower($result->kontraktor_pelaksana));
            $data["create_at"] = $result->create_at;
            $data["status"] = "kontraktor";
            return $this->response_sukses($data);
        }else{
            // check di tabel sys user
            $result2 = $this->get_data_admin($param->param->id, $param->param->password);
                if($result2->num_rows() > 0){
                $result2 = $result2->row();
                $data["userid"] = $result2->id;
                $data["username"] = $result2->username;
                $data["pin"] = $result2->pin;
                $data["nama"] = ucwords(strtolower($result2->pengawas_nama));
                $data["direktur"] = ucwords(strtolower($result2->pengawas_direktur));
                $data["alamat"] = "";
                $data["kontraktor_pelaksana"] = "";
                $data["create_at"] = $result2->create_at;
                $data["status"] = "admin";
                return $this->response_sukses($data);
            }else{
                // check di tabel sys user
                $result = $this->get_data_admin($param->param->id, $param->param->password);
                return $this->response_gagal("02", "Akun pengguna atau kata sandi tidak valid");die();
            }
        }
    }
    
    public function update_data($param){
        if(empty($param->param->id)){
            return $this->response_gagal("02", "ID tidak tersedia");die();
        }
        if(empty($param->param->token)){
            return $this->response_gagal("02", "Token tidak tersedia");die();
        }
       
        $id = $param->param->id;
        $token = $param->param->token;
        $tipe_user = $param->user_type;
        // check id exist
        $query = "SELECT id_user FROM firebase_user where userid = ? and tipe_user = ?";
        $result = $this->db_prod->query($query, array($id, $tipe_user));
        if($result->num_rows() > 0){
            $data_row = $result->row();
            $id_usernya = $data_row->id_user;
            // update tabel nya
            $updatedata = array(
                "fcmregid" => $token,
            );
            $update = $this->processing_update_data("firebase_user", "id_user", $id_usernya, $updatedata);
            if(!$update){
                return $this->response_gagal("02", "Proses gagal, silahkan ulangi kembali");
            }else{
                return $this->response_sukses("Proses Edit Data Berhasil");
            }
        }else{
            // insert ke tabel
            $insertdata = array(
                "userid" => $id,
                "fcmregid" => $token,
                "tipe_user" => $tipe_user,
                "date_created" => date('Y-m-d H:i:s')
            );
            $insert = $this->db_prod->insert("firebase_user", $insertdata);
            if(!$insert){
                 return $this->response_gagal("02", "Proses gagal, silahkan ulangi kembali");
            }else{
                return $this->response_sukses("Proses Tambah Data Berhasil, ID ".$insert);
            }
        }
    }
    
    public function check_version($param){
        $url_update = "https://api.pupr.probolinggokab.go.id/index.php/app";
        if(empty($param->param->version)){
            return $this->response_gagal("02", "Version tidak tersedia");die();
        }
        $version_app = str_replace(array(".",",","#"), array("","",""), $param->param->version);
        $query = "SELECT apps_version FROM app_downloader order by release_date desc limit 1";
        $result = $this->db_prod->query($query);
        if($result->num_rows() > 0){
            $data_row = $result->row();
            $versi = str_replace(array(".",",","#"), array("","",""), $data_row->apps_version);
            if(intval($version_app) < intval($versi)){
                return $this->response_gagal("02", "Versi terbaru sudah tersedia, Silahkan update aplikasi Anda", $url_update);die();
            }else{
                return $this->response_sukses("success");
            }
        }else{
            // dianggap versi awal
            return $this->response_sukses("success");
        }
    }
    
    public function download_panduan($param){
        $this->load->helper('download');
        header("Content-type: application/pdf");
        file_get_contents("http://127.0.0.1/api_prob/assets/panduan.pdf");
//        force_download("panduan.pdf", $data);
    }
    
    public function insert_log($param){
        if(empty($param->param->id)){
            return $this->response_gagal("02", "ID tidak tersedia");die();
        }
        if(empty($param->param->tipe)){
            return $this->response_gagal("02", "Tipe tidak tersedia");die();
        }
        if(empty($param->param->device_info)){
            return $this->response_gagal("02", "Device info tidak tersedia");die();
        }
        
        $id = $param->param->id;
        $tipe = $param->param->tipe;
        $device = $param->param->device_info;
        $ket = empty($param->param->keterangan) ? "" : $param->param->keterangan;
        
        $insertdata = array(
            "userid" => $id,
            "tipe" => strtolower($tipe),
            "device_info" => $device,
            "keterangan" => $ket,
            "created_at" => date('Y-m-d H:i:s')
        );
        $insert = $this->db_prod->insert("log_mobile", $insertdata);
        if(!$insert){
             return $this->response_gagal("02", "Proses gagal, silahkan ulangi kembali");
        }else{
            return $this->response_sukses("Proses Tambah Data Berhasil, ID ".$insert);
        }
    }
    
    protected function get_data($id, $pass)
    {
        $query = "SELECT * FROM kontraktor where username = ? and pin = ?";
        return $this->db_prod->query($query, array($id, hash('sha256', $pass)));
    }
    
    protected function get_data_admin($id, $pass)
    {
        $query = "SELECT * FROM pengawas where username = ? and pin = ? and status = 1";
        return $this->db_prod->query($query, array($id, hash('sha256', $pass)));
    }
}