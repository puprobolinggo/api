<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Proyek model
============================================================== */
class Proyek extends MY_Model {
    
    public function __construct() {
        parent::__construct();
    }
    public function proyek_terbaru($param){
        if(empty($param->param->kontraktor_id)){
            return $this->response_gagal("02", "Kontraktor ID tidak tersedia");die();
        }
        $id = $param->param->kontraktor_id;
        $limit = $param->param->limit;
        $user_type = empty($param->user_type) ? "" : $param->user_type;
        
        if(strtolower($user_type) != "admin"){
            $query = "SELECT * FROM pekerjaan where kontraktor_id = ?";
            if($limit != ""){
                $query = "SELECT * FROM pekerjaan where kontraktor_id = ? order by pekerjaan_id desc LIMIT $limit";
            }
        }else{
            $query = "SELECT * FROM pekerjaan where pengawas_id = ?";
            if($limit != ""){
                $query = "SELECT * FROM pekerjaan where pengawas_id = ? order by pekerjaan_id desc LIMIT $limit";
            }
        }
        
        $result = $this->db_prod->query($query, $id);
        if($result->num_rows() > 0){
            $no = 0;
            foreach($result->result() as $row){
                $tgl_kontrak = $this->format_date_indo($row->kontrak_tanggal);
                if($tgl_kontrak == "01 Januari 1970"){
                    $tgl_kontrak = "";
                }
                 
                $tgl_mulai = $this->format_date_indo($row->kontrak_mulai);
                if($tgl_mulai == "01 Januari 1970"){
                    $tgl_mulai = "";
                }
                
                $tgl_selesai = $this->format_date_indo($row->kontrak_selesai);
                if($tgl_selesai == "01 Januari 1970"){
                    $tgl_selesai = "";
                }
                $resp[] = array(
                    "pekerjaan_id" => $row->pekerjaan_id,
                    "pekerjaan_nama" => $row->pekerjaan_nama,
                    "kontrak_no" => $row->kontrak_no,
                    "kontrak_tanggal" => $tgl_kontrak,
                    "kontrak_mulai" => $tgl_mulai,
                    "kontrak_selesai" => $tgl_selesai,
                    "kontrak_nilai" => $row->kontrak_nilai,
                    "asisten" => $row->asisten,
                    "asisten_nip" => $row->asisten_nip,
                    "status" => $row->status,
                );
                $no++;
            }
            
            $response = array(
                "jumlah" => $no,
                "detail" => $resp,
            );
            
            // create respon html
            $html = "";
            foreach($response['detail'] as $row){
                $status = $row['status'];
                $icon = "box-default";
                if(strtolower($status) == 'belum dimulai'){
                    $icon = "box-default";
                }else if(strtolower($status) == 'dalam pengerjaan'){
                    $icon = "box-default";
                }else if(strtolower($status) == 'selesai'){
                    $icon = "box-default";
                }else{
                    $icon = "box-default";
                }
                
                $html .= "<div class='col-md-12 col-xs-12'>
                            <div class='box ".$icon." box-solid'>
                                <div class='box-header with-border'>
                                    <a href='#/pekerjaan_detail/".$row['pekerjaan_id']."'>
                                        <i style='font-style: normal; font-size: 14px;margin-right:10px; font-weight:bold;'>".ucwords(strtolower($status))."</i>
                                            <span class='fa fa-1x fa-angle-double-right  pull-right' style='margin-top:2px; margin-left:5px;'></span>
                                        <i class='pull-right' style='font-style: normal; font-size: 12px;'>Lihat Detail</i>
                                            
                                    </a>    
                                </div>
                                <div class='box-body'>
                                    <a href='#/pekerjaan_detail/".$row['pekerjaan_id']."' style='color:#333;'>
                                        <span class='fa fa-1x fa-calendar-plus-o'></span>
                                        <i style='font-style: normal; font-size: 12px'>$tgl_mulai</i>
                                        -
                                        <i style='font-style: normal; font-size: 12px'>$tgl_selesai</i>
                                        <p class='title-pekerjaan'><b>".ucwords(strtolower($row['kontrak_no']))."</b></p>
                                        <p class='title-pekerjaan'>".ucwords(strtolower($row['pekerjaan_nama']))."</p>
                                    </a> 
                                    <a href='#/pekerjaan_detail_rab/$id' class='btn btn-block btn-social btn-twitter btn-flat' style='margin-top:10px;'>
                                        <i class='fa fa-cloud-upload'></i> Upload Dokumen File RAB
                                    </a>
                                    
                                </div>
                            </div>
                            
                            </div>";
            }
            
            return $this->response_sukses($response, $html);
        }else{
            return $this->response_gagal_html("02", "Tidak ada berita terbaru");die();
        }
    }
    
    public function proyek_aktif($param){
        if(empty($param->param->kontraktor_id)){
            return $this->response_gagal("02", "Kontraktod ID tidak tersedia");die();
        }
        $id = $param->param->kontraktor_id;
        $limit = $param->param->limit;
        $user_type = empty($param->user_type) ? "" : $param->user_type;
        
        if(strtolower($user_type) != "admin"){
            $query = "SELECT * FROM pekerjaan where kontraktor_id = ? and status = 'dalam pengerjaan'";
            if($limit != ""){
                $query = "SELECT * FROM pekerjaan where kontraktor_id = ? and status = 'dalam pengerjaan' order by pekerjaan_id desc LIMIT $limit";
            }
        }else{
            $query = "SELECT * FROM pekerjaan where pengawas_id = ? and status = 'dalam pengerjaan'";
            if($limit != ""){
                $query = "SELECT * FROM pekerjaan where pengawas_id = ? and status = 'dalam pengerjaan' order by pekerjaan_id desc LIMIT $limit";
            }
        }
        
        $result = $this->db_prod->query($query, $id);
        if($result->num_rows() > 0){
            $no = 0;
            foreach($result->result() as $row){
                $tgl_kontrak = $this->format_date_indo($row->kontrak_tanggal);
                if($tgl_kontrak == "01 Januari 1970"){
                    $tgl_kontrak = "";
                }
                 
                $tgl_mulai = $this->format_date_indo($row->kontrak_mulai);
                if($tgl_mulai == "01 Januari 1970"){
                    $tgl_mulai = "";
                }
                
                $tgl_selesai = $this->format_date_indo($row->kontrak_selesai);
                if($tgl_selesai == "01 Januari 1970"){
                    $tgl_selesai = "";
                }
                $resp[] = array(
                    "pekerjaan_id" => $row->pekerjaan_id,
                    "pekerjaan_nama" => $row->pekerjaan_nama,
                    "kontrak_no" => $row->kontrak_no,
                    "kontrak_tanggal" => $tgl_kontrak,
                    "kontrak_mulai" => $tgl_mulai,
                    "kontrak_selesai" => $tgl_selesai,
                    "kontrak_nilai" => $row->kontrak_nilai,
                    "asisten" => $row->asisten,
                    "asisten_nip" => $row->asisten_nip,
                    "status" => $row->status,
                );
                $no++;
            }
            
            $response = array(
                "jumlah" => $no,
                "detail" => $resp,
            );
            
            // create respon html
            $html = "";
            $no = 1;
            foreach($response['detail'] as $row){
                $pekerjaan_id = $row['pekerjaan_id'];
                
                if(strtolower($user_type) != "admin"){
                    $html .= "<div class='col-md-12 col-xs-12'>
                                <p class='title-pekerjaan'><b>".$no.".&nbsp;".ucwords(strtolower($row['pekerjaan_nama']))."</b></p>
                            </div>
                            <div class='col-md-3 col-xs-3'>
                                <p class='title-pekerjaan'>
                                    Kontrak
                                </p>
                            </div> 
                            <div class='col-md-9 col-xs-9'>
                                <p class='title-pekerjaan'>
                                    ".ucwords(strtolower($row['kontrak_no']))."
                                </p>
                            </div>
                            <div class='col-md-3 col-xs-3'>
                                <p class='title-pekerjaan'>
                                    Periode
                                </p>
                            </div> 
                            <div class='col-md-9 col-xs-9'>
                                <span class='fa fa-1x fa-calendar-plus-o'></span>
                                <i style='font-style: normal; font-size: 12px'>".$row['kontrak_mulai']."</i>
                                -
                                <i style='font-style: normal; font-size: 12px'>".$row['kontrak_selesai']."</i>
                            </div>

                            <div class='col-md-6 col-xs-6' style='margin-top: 10px; padding-right:5px;'>
                                <a href='#/laporan_aktifitas_detail_pekerjaan/$pekerjaan_id' class='btn btn-block btn-social btn-facebook btn-flat btn-sm'>
                                    <i class='fa fa-list-alt'></i> Lihat Detail Pekerjaan
                                </a>
                            </div>
                            <div class='col-md-6 col-xs-6' style='margin-top: 10px; padding-left:5px;'>
                                <a href='#/laporan_aktifitas_detail_aktifitas/$pekerjaan_id' class='btn btn-block btn-social btn-dropbox btn-flat btn-sm'>
                                    <i class='fa fa-camera-retro'></i> Lihat Detail Aktifitas
                                </a>
                            </div>";
                }else{
                    $html .= "<div class='col-md-12 col-xs-12'>
                                <p class='title-pekerjaan'><b>".$no.".&nbsp;".ucwords(strtolower($row['pekerjaan_nama']))."</b></p>
                            </div>
                            <div class='col-md-3 col-xs-3'>
                                <p class='title-pekerjaan'>
                                    Kontrak
                                </p>
                            </div> 
                            <div class='col-md-9 col-xs-9'>
                                <p class='title-pekerjaan'>
                                    ".ucwords(strtolower($row['kontrak_no']))."
                                </p>
                            </div>
                            <div class='col-md-3 col-xs-3'>
                                <p class='title-pekerjaan'>
                                    Periode
                                </p>
                            </div> 
                            <div class='col-md-9 col-xs-9'>
                                <span class='fa fa-1x fa-calendar-plus-o'></span>
                                <i style='font-style: normal; font-size: 12px'>".$row['kontrak_mulai']."</i>
                                -
                                <i style='font-style: normal; font-size: 12px'>".$row['kontrak_selesai']."</i>
                            </div>

                            <div class='col-md-6 col-xs-6' style='margin-top: 10px; padding-right:5px;'>
                                <a href='#/laporan_aktifitas_detail_pekerjaan/$pekerjaan_id' class='btn btn-block btn-social btn-facebook btn-flat btn-sm'>
                                    <i class='fa fa-list-alt'></i> Lihat Detail Pekerjaan
                                </a>
                            </div>
                            <div class='col-md-6 col-xs-6' style='margin-top: 10px; padding-left:5px;'>
                                <a href='#/laporan_aktifitas_detail_aktifitas/$pekerjaan_id' class='btn btn-block btn-social btn-dropbox btn-flat btn-sm'>
                                    <i class='fa fa-camera-retro'></i> Lihat Detail Aktifitas
                                </a>
                            </div>
                            <div class='col-md-12 col-xs-12' style='margin-top: 10px;'>
                                <a href='#/laporan_aktifitas_detail_aktifitas_verifikasi/$pekerjaan_id' class='btn btn-block btn-social btn-google btn-flat btn-sm'>
                                    <i class='fa fa-calendar-check-o'></i> Lihat Daftar Aktifitas Pekerjaan yang Belum di Verifikasi
                                </a>
                            </div>";
                }
                $no++;
            }
            
            return $this->response_sukses($response, $html);
        }else{
            return $this->response_gagal("02", "Tidak ada berita terbaru");die();
        }
    }
    
    public function proyek_detail($param){
        if(empty($param->param->id)){
            return $this->response_gagal("02", "ID pekerjaan tidak tersedia");die();
        }
        $id = $param->param->id;
        $query = "SELECT k.kegiatan_nama, k.kegiatan_lokasi, k.sumber_dana, k.created_at, k.updated_at,
            kk.nama, kk.direktur, kk.alamat, kk.kontraktor_pelaksana,
            p.pengawas_nama as nama_pengawas, p.pengawas_direktur, p.pengawas, p.pengawas_pelaksana,
            pp.pekerjaan_id, pp.pekerjaan_nama, pp.kontrak_no, pp.kontrak_tanggal,
            pp.kontrak_mulai, pp.kontrak_selesai, pp.kontrak_nilai,
            pp.asisten, pp.asisten_nip, pp.status
            FROM pekerjaan pp
            LEFT JOIN kegiatan k on pp.kegiatan_id = k.id
            left join kontraktor kk on pp.kontraktor_id = kk.id
            left join pengawas p on pp.pengawas_id = p.id
            where pp.pekerjaan_id = ?";
        $result = $this->db_prod->query($query, $id);
        if($result->num_rows() > 0){
            $row = $result->row();
            
            $tgl_kontrak = $this->format_date_indo($row->kontrak_tanggal);
            if($tgl_kontrak == "01 Januari 1970"){
                $tgl_kontrak = "";
            }

            $tgl_mulai = $this->format_date_indo($row->kontrak_mulai);
            if($tgl_mulai == "01 Januari 1970"){
                $tgl_mulai = "";
            }

            $tgl_selesai = $this->format_date_indo($row->kontrak_selesai);
            if($tgl_selesai == "01 Januari 1970"){
                $tgl_selesai = "";
            }
            
            $response = array(
                "pekerjaan_id" => $row->pekerjaan_id,
                "pekerjaan_nama" => ucwords(strtolower($row->pekerjaan_nama)),
                "kontrak_no" => $row->kontrak_no,
                "kontrak_tanggal" => $tgl_kontrak,
                "kontrak_mulai" => $tgl_mulai,
                "kontrak_selesai" => $tgl_selesai,
                "kontrak_nilai" => $row->kontrak_nilai,
                "asisten" => ucwords(strtolower($row->asisten)),
                "asisten_nip" => $row->asisten_nip,
                "status" => $row->status,
                
                "kegiatan_nama" => ucwords(strtolower($row->kegiatan_nama)),
                "kegiatan_lokasi" => ucwords(strtolower($row->kegiatan_lokasi)),
                "kegiatan_sumber_dana" => (strtoupper($row->sumber_dana)),
                "kegiatan_created_at" => $row->created_at,
                "kegiatan_updated_at" => $row->updated_at,
                "kontraktor_nama" => ucwords(strtolower($row->nama)),
                "kontraktor_direktur" => ucwords(strtolower($row->direktur)),
                "kontraktor_pelaksana" => ucwords(strtolower($row->kontraktor_pelaksana)),
                "kontraktor_alamat" => ucwords(strtolower($row->alamat)),
                "pengawas_nama" => ucwords(strtolower($row->nama_pengawas)),
                "pengawas_direktur" => ucwords(strtolower($row->direktur)),
                "pengawas" => ucwords(strtolower($row->pengawas)),
                "pengawas_pelaksana" => ucwords(strtolower($row->pengawas_pelaksana)),
            );
            return $this->response_sukses($response);
        }else{
            return $this->response_gagal("02", "Tidak ada berita terbaru");die();
        }
    }
    
    public function pekerjaan_detail($param){
        if(empty($param->param->id)){
            return $this->response_gagal("02", "ID pekerjaan tidak tersedia");die();
        }
        $id = $param->param->id;
        $query = "SELECT * FROM pekerjaan_sub where pekerjaan_id = ? order by pekerjaan_sub_id asc";
        $result = $this->db_prod->query($query, $id);
        if($result->num_rows() > 0){
            $response = array();
            $no = 1;
            $html = "";
            foreach($result->result() as $row){
                $pekerjaan_sub_id = $row->pekerjaan_sub_id;
                $pekerjaan_sub_nama = ucwords(strtolower($row->pekerjaan_sub_nama));

                $response[] = array(
                    "pekerjaan_id" => $row->pekerjaan_id,
                    "pekerjaan_sub_id" => $row->pekerjaan_sub_id,
                    "pekerjaan_sub_nama" => ucwords(strtolower($row->pekerjaan_sub_nama)),
                    "updated_at" => $row->updated_at
                );
                
                $html .= "<div class='col-md-12 col-xs-12'>
                            <div class='box box-default box-solid' style='box-shadow:0px;'>
                                <div class='box-header with-border box-header-small'>
                                    <a href='#/laporan_aktifitas_detail_pekerjaan_edit/$row->pekerjaan_id/$pekerjaan_sub_id/$pekerjaan_sub_nama' style='color: #333;'>
                                        <span class='fa fa-1x fa-pencil'></span>
                                        <i style='font-style: normal; font-size: 12px'>Edit data    </i>
                                    </a>
                                    &nbsp;&nbsp;
                                    <a href='#/laporan_aktifitas_detail_pekerjaan_sub/$pekerjaan_sub_id/$pekerjaan_sub_nama/$row->pekerjaan_id' style='color: #333;'>
                                        <span class='fa fa-1x fa-angle-double-right  pull-right' style='margin-top:2px; margin-left:5px;'></span>
                                        <i class='pull-right' style='font-style: normal; font-size: 12px;'>Lihat Uraian Pekerjaan</i>
                                    </a>
                                </div>
                                <div class='box-body'>
                                    <a href='#/laporan_aktifitas_detail_pekerjaan_sub/$pekerjaan_sub_id/$pekerjaan_sub_nama/$row->pekerjaan_id'>
                                        <p class='title-pekerjaan'><b>".ucwords(strtolower($row->pekerjaan_sub_nama))."</b></p>
                                    </a>
                                </div>
                            </div>
                        </div>";
                
                $no++;
            }
            return $this->response_sukses($response, $html);
        }else{
            return $this->response_gagal("02", "Belum ada detail pekerjaan, silahkan tambahkan detail pekerjaan Anda.");die();
        }
    }
    
    public function pekerjaan_detail_add($param){
        if(empty($param->param->id)){
            return $this->response_gagal("02", "ID pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->nama)){
            return $this->response_gagal("02", "Nama pekerjaan tidak tersedia");die();
        }
       
        $insertdata = array(
            "pekerjaan_id" => $param->param->id,
            "pekerjaan_sub_nama" => strtoupper($param->param->nama),
            "updated_at" => date('Y-m-d H:i:s')
        );
        
        $insert = $this->db_prod->insert("pekerjaan_sub", $insertdata);
        if(!$insert){
             return $this->response_gagal("02", "Proses gagal, silahkan ulangi kembali");
        }else{
            return $this->response_sukses("Proses Tambah Data Berhasil, ID ".$insert);
        }
    }
    
    public function pekerjaan_detail_edit($param){
        if(empty($param->param->id)){
            return $this->response_gagal("02", "ID Sub pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->nama)){
            return $this->response_gagal("02", "Nama pekerjaan tidak tersedia");die();
        }
      
        $updatedata = array(
            "pekerjaan_sub_nama" => strtoupper($param->param->nama),
            "updated_at" => date('Y-m-d H:i:s')
        );
        
        $update = $this->processing_update_data("pekerjaan_sub", "pekerjaan_sub_id", $param->param->id, $updatedata);
        if(!$update){
             return $this->response_gagal("02", "Proses gagal, silahkan ulangi kembali");
        }else{
            return $this->response_sukses("Proses Edit Data Berhasil");
        }
    }
    
    public function pekerjaan_detail_sub($param){
        if(empty($param->param->id)){
            return $this->response_gagal("02", "ID pekerjaan tidak tersedia");die();
        }
        $id = $param->param->id;
        $pekerjaan_id = $param->param->pekerjaan_id;
        
        
        $query = "SELECT * FROM pekerjaan_uraian where pekerjaan_sub_id = ? order by pekerjaan_uraian_id asc";
        $result = $this->db_prod->query($query, $id);
        if($result->num_rows() > 0){
            $response = array();
            $no = 1;
            $html = "";
            foreach($result->result() as $row){
                $pekerjaan_sub_id = $row->pekerjaan_sub_id == "" ? "-" : $row->pekerjaan_sub_id;
                $pekerjaan_uraian_id = $row->pekerjaan_uraian_id == "" ? "-" : $row->pekerjaan_uraian_id;
                $pekerjaan_uraian_nama = ucwords(strtolower($row->pekerjaan_uraian_nama)) == "" ? "-" : ucwords(strtolower($row->pekerjaan_uraian_nama));
                $pekerjaan_uraian_satuan = $row->pekerjaan_uraian_satuan == "" ? "-" : $row->pekerjaan_uraian_satuan;
                $pekerjaan_uraian_volume = $row->pekerjaan_uraian_volume == "" ? "-" : $row->pekerjaan_uraian_volume;
                $pekerjaan_uraian_volume_update = $row->pekerjaan_uraian_volume_update == "" ? "-" : $row->pekerjaan_uraian_volume_update;
                $pekerjaan_uraian_bobot = $row->pekerjaan_uraian_bobot == "" ? "-" : $row->pekerjaan_uraian_bobot;
                $pekerjaan_uraian_harga = $row->pekerjaan_uraian_harga == "" ? "-" : $row->pekerjaan_uraian_harga;
                $pekerjaan_uraian_total = $row->pekerjaan_uraian_total == "" ? "-" : $row->pekerjaan_uraian_total;
                $pekerjaan_uraian_keterangan = $row->pekerjaan_uraian_keterangan == "" ? "-" : $row->pekerjaan_uraian_keterangan;
                
                $pekerjaan_uraian_nama = str_replace("/", ",", $pekerjaan_uraian_nama);
                
                $response[] = array(
                    "pekerjaan_sub_id" => $row->pekerjaan_sub_id,
                    "pekerjaan_uraian_id" => $row->pekerjaan_uraian_id,
                    "pekerjaan_uraian_nama" => ucwords(strtolower($row->pekerjaan_uraian_nama)),
                    "pekerjaan_uraian_satuan" => $row->pekerjaan_uraian_satuan,
                    "pekerjaan_uraian_volume" => $row->pekerjaan_uraian_volume,
                    "pekerjaan_uraian_volume_update" => $row->pekerjaan_uraian_volume_update,
                    "pekerjaan_uraian_bobot" => $row->pekerjaan_uraian_bobot,
                    "pekerjaan_uraian_harga" => $row->pekerjaan_uraian_harga,
                    "pekerjaan_uraian_total" => $row->pekerjaan_uraian_total,
                    "pekerjaan_uraian_keterangan" => $row->pekerjaan_uraian_keterangan,
                    "pekerjaan_uraian_created_at" => $row->pekerjaan_uraian_created_at,
                    "pekerjaan_uraian_updated_at" => $row->pekerjaan_uraian_updated_at,
                );
                
                $html .= "<div class='col-md-12 col-xs-12' style='margin-bottom:5px;'>
                                <p class='title-pekerjaan'>".$no.". ".ucwords(strtolower($row->pekerjaan_uraian_nama))."
                                    <a href='#/laporan_aktifitas_detail_pekerjaan_sub_edit/$pekerjaan_sub_id/$pekerjaan_uraian_id/".$pekerjaan_uraian_nama."/$pekerjaan_uraian_satuan/$pekerjaan_uraian_volume/$pekerjaan_uraian_bobot/$pekerjaan_uraian_harga/$pekerjaan_uraian_total/$pekerjaan_uraian_keterangan/$pekerjaan_id' style='color: #333;' class='pull-right'>
                                        <span class='fa fa-1x fa-pencil'></span>
                                        <i style='font-style: normal; font-size: 12px'>Edit</i>
                                    </a>
                                </p>
                            </div>
                            <div class='col-md-12 col-xs-12'>
                                <table class='table table-bordered'>
                                    <tr>
                                        <td><center>Satuan</center></td>
                                        <td><center>Volume</center></td>
                                        <td><center>Bobot</center></td>
                                        <td><center>Ket</center></td>
                                    </tr>
                                    <tr>
                                        <td><center>".$row->pekerjaan_uraian_satuan."</center></td>
                                        <td><center>".$row->pekerjaan_uraian_volume."</center></td>
                                        <td><center>".$row->pekerjaan_uraian_bobot."</center></td>
                                        <td><center><i style='background-color: #00a157; padding: 3px 7px; color: #fff; border-radius: 3px; font-size: 11px;'>".$row->pekerjaan_uraian_keterangan."</i></center></td>
                                    </tr>
                                </table>
                            </div>
                            <div class='col-md-6 col-xs-6' style='margin-top: -15px; margin-bottom:10px;'>
                                Harga : ".  number_format($row->pekerjaan_uraian_harga, 0 , ',', '.')."
                            </div>
                            <div class='col-md-6 col-xs-6' style='margin-top: -15px; margin-bottom:10px;'>
                                Total : ".  number_format($row->pekerjaan_uraian_total, 0, ',', '.')."
                            </div>
                            ";
                $no++;
            }
            return $this->response_sukses($response, $html);
        }else{
            return $this->response_gagal_html("02", "Tidak ada berita terbaru");die();
        }
    }
    
    public function pekerjaan_detail_sub_add($param){
        if(empty($param->param->id)){
            return $this->response_gagal("02", "ID pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->nama)){
            return $this->response_gagal("02", "Nama uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->satuan)){
            return $this->response_gagal("02", "Satuan uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->volume)){
            return $this->response_gagal("02", "Volume uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->bobot)){
            return $this->response_gagal("02", "Bobot uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->harga)){
            return $this->response_gagal("02", "Harga uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->total)){
            return $this->response_gagal("02", "Total uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->keterangan)){
            return $this->response_gagal("02", "Keterangan uraian pekerjaan tidak tersedia");die();
        }
        if (!preg_match('/^[0-9]+(\\.[0-9]+)?$/', $param->param->volume)){
            return $this->response_gagal("02", "Format nilai volume salah");die();
        }
        
        $insertdata = array(
            "pekerjaan_sub_id" => $param->param->id,
            "pekerjaan_uraian_nama" => ucwords(strtoupper($param->param->nama)),
            "pekerjaan_uraian_satuan" => $param->param->satuan,
            "pekerjaan_uraian_volume" => $param->param->volume,
            "pekerjaan_uraian_bobot" => str_replace(".", "", $param->param->bobot),
            "pekerjaan_uraian_harga" => str_replace(".", "", $param->param->harga),
            "pekerjaan_uraian_total" => str_replace(".", "", $param->param->total),
            "pekerjaan_uraian_keterangan" => $param->param->keterangan,
            "pekerjaan_uraian_created_at" => date('Y-m-d H:i:s')
        );
        $insert = $this->db_prod->insert("pekerjaan_uraian", $insertdata);
        if(!$insert){
             return $this->response_gagal("02", "Proses gagal, silahkan ulangi kembali");
        }else{
            return $this->response_sukses("Proses Tambah Data Berhasil, ID ".$insert);
        }
    }
    public function pekerjaan_detail_sub_edit($param){
        if($param->param->id == ""){
            return $this->response_gagal("02", "ID Uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->nama)){
            return $this->response_gagal("02", "Nama uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->satuan)){
            return $this->response_gagal("02", "Satuan uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->volume)){
            return $this->response_gagal("02", "Volume uraian pekerjaan tidak tersedia");die();
        }
//        if(empty($param->param->bobot)){
//            return $this->response_gagal("02", "Bobot uraian pekerjaan tidak tersedia");die();
//        }
        if(empty($param->param->harga)){
            return $this->response_gagal("02", "Harga uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->total)){
            return $this->response_gagal("02", "Total uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->keterangan)){
            return $this->response_gagal("02", "Keterangan uraian pekerjaan tidak tersedia");die();
        }
       
        $updatedata = array(
            "pekerjaan_uraian_nama" => ucwords(strtoupper($param->param->nama)),
            "pekerjaan_uraian_satuan" => $param->param->satuan,
            "pekerjaan_uraian_volume" => $param->param->volume,
            "pekerjaan_uraian_bobot" => str_replace(".", "", $param->param->bobot),
            "pekerjaan_uraian_harga" => str_replace(".", "", $param->param->harga),
            "pekerjaan_uraian_total" => str_replace(".", "", $param->param->total),
            "pekerjaan_uraian_keterangan" => $param->param->keterangan,
            "pekerjaan_uraian_updated_at" => date('Y-m-d H:i:s')
        );
        
        $update = $this->processing_update_data("pekerjaan_uraian", "pekerjaan_uraian_id", $param->param->id, $updatedata);
        if(!$update){
             return $this->response_gagal("02", "Proses gagal, silahkan ulangi kembali");
        }else{
            return $this->response_sukses("Proses Edit Data Berhasil");
        }
    }
}