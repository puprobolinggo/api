<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Aktifitas model
============================================================== */
class Aktifitas extends MY_Model {
    
    protected $api_url;
    public function __construct() {
        parent::__construct();
        
        $this->api_url = "http://localhost:21080";
//        $this->api_url = "http://103.93.55.29:21080";
    }
    
    public function aktifitas_detail($param){
        if(empty($param->param->id)){
            return $this->response_gagal("02", "ID pekerjaan tidak tersedia");die();
        }
        $id = $param->param->id;
        $query = "SELECT p.pekerjaan_uraian_detail_id, p.kegiatan_id, p.pekerjaan_id, p.pekerjaan_sub_id, p.pejabat_id,
                p.pekerjaan_uraian_id,p.pekerjaan_uraian_detail_keterangan, 
                p.pekerjaan_uraian_detail_rumus, p.pekerjaan_uraian_detail_verificator, p.pekerjaan_uraian_detail_creator,
                p.pekerjaan_uraian_detail_sketsa, p.pekerjaan_uraian_detail_create_at,
                pu.pekerjaan_uraian_nama,
                pj.pejabat_nip,pj.pejabat_nama, pj.jabatan, pj.tipe_pejabat
                FROM pekerjaan_uraian_detail p
                left join pekerjaan_uraian pu on p.pekerjaan_uraian_id = pu.pekerjaan_uraian_id
                left join pejabat pj on p.pejabat_id = pj.pejabat_id
                where p.pekerjaan_id = ?
                order by p.pekerjaan_uraian_detail_id desc";
        $result = $this->db_prod->query($query, $id);
        if($result->num_rows() > 0){
            $response = array();
            $no = 1;
            $html = "";
            foreach($result->result() as $row){
                $pekerjaan_uraian_detail_id = $row->pekerjaan_uraian_detail_id;
                $pekerjaan_uraian_nama = ucwords(strtolower($row->pekerjaan_uraian_nama));

                $response[] = array(
                    "pekerjaan_uraian_detail_id" => $row->pekerjaan_uraian_detail_id,
                    "kegiatan_id" => $row->kegiatan_id,
                    "pekerjaan_id" => $row->pekerjaan_id,
                    "pekerjaan_sub_id" => $row->pekerjaan_sub_id,
                    "pejabat_id" => $row->pejabat_id,
                    "pekerjaan_uraian_id" => $row->pekerjaan_uraian_id,
                    "pekerjaan_uraian_detail_keterangan" => $row->pekerjaan_uraian_detail_keterangan,
                    "pekerjaan_uraian_detail_fields" => $row->pekerjaan_uraian_detail_fields,
                    "pekerjaan_uraian_detail_rumus" => $row->pekerjaan_uraian_detail_rumus,
                    "pekerjaan_uraian_detail_verificator" => $row->pekerjaan_uraian_detail_verificator,
                    "pekerjaan_uraian_detail_creator" => $row->pekerjaan_uraian_detail_creator,
                    "pekerjaan_uraian_detail_sketsa" => $row->pekerjaan_uraian_detail_sketsa,
                    "pekerjaan_uraian_detail_create_at" => $row->pekerjaan_uraian_detail_create_at,
                    
                    "pekerjaan_uraian_nama" => $row->pekerjaan_uraian_nama,
                    
                    "pejabat_nip" => $row->pejabat_nip,
                    "pejabat_nama" => $row->pejabat_nama,
                    "jabatan" => $row->jabatan,
                    "tipe_pejabat" => $row->tipe_pejabat,
                );
                
                $html .= "<div class='col-md-12 col-xs-12'>
                            <div class='box box-default box-solid' style='box-shadow:0px;'>
                                <div class='box-header with-border box-header-small'>
                                    <a href='#/laporan_aktifitas_detail_aktifitas_edit/$row->pekerjaan_id/$row->pekerjaan_uraian_detail_id' style='color: #333;'>
                                        <span class='fa fa-1x fa-pencil'></span>
                                        <i style='font-style: normal; font-size: 12px'>Edit data    </i>
                                    </a>
                                    &nbsp;&nbsp;
                                    <a href='#/laporan_aktifitas_detail_aktifitas_sub/$row->kegiatan_id/$row->pekerjaan_id/$row->pekerjaan_sub_id/$row->pekerjaan_uraian_id/$row->pekerjaan_uraian_detail_id' style='color: #333;'>
                                        <span class='fa fa-1x fa-angle-double-right  pull-right' style='margin-top:2px; margin-left:5px;'></span>
                                        <i class='pull-right' style='font-style: normal; font-size: 12px;'>Lihat Detail Uraian Aktifitas</i>
                                    </a>
                                </div>
                                <div class='box-body'>
                                    <a href='#/laporan_aktifitas_detail_aktifitas_sub/$row->kegiatan_id/$row->pekerjaan_id/$row->pekerjaan_sub_id/$row->pekerjaan_uraian_id/$row->pekerjaan_uraian_detail_id'>
                                        <p class='title-pekerjaan'><b>".ucwords(strtolower($row->pekerjaan_uraian_nama))."</b></p>
                                    </a>
                                </div>
                            </div>
                        </div>";
//                $html .= "<div class='col-md-12 col-xs-12'>
//                            <div class='box box-default box-solid' style='box-shadow:0px;'>
//                                <div class='box-header with-border box-header-small'>
//                                    &nbsp;&nbsp;
//                                    <a href='#/laporan_aktifitas_detail_aktifitas_sub/$row->kegiatan_id/$row->pekerjaan_id/$row->pekerjaan_sub_id/$row->pekerjaan_uraian_id/$row->pekerjaan_uraian_detail_id' style='color: #333;'>
//                                        <i class='pull-left' style='font-style: normal; font-size: 12px;'>Lihat Detail Uraian Aktifitas</i>
//                                    
//                                        <span class='fa fa-1x fa-angle-double-right  pull-left' style='margin-top:2px; margin-left:5px;'></span>
//                                    </a>
//                                </div>
//                                <div class='box-body'>
//                                    <a href='#/laporan_aktifitas_detail_aktifitas_sub/$row->kegiatan_id/$row->pekerjaan_id/$row->pekerjaan_sub_id/$row->pekerjaan_uraian_id/$row->pekerjaan_uraian_detail_id'>
//                                        <p class='title-pekerjaan'><b>".ucwords(strtolower($row->pekerjaan_uraian_nama))."</b></p>
//                                    </a>
//                                </div>
//                            </div>
//                        </div>";
                
                $no++;
            }
            return $this->response_sukses($response, $html);
        }else{
            return $this->response_gagal_html("02", "<div class='col-md-12 col-xs-12'>Belum ada detail aktifitas, silahkan tambahkan detail aktifitas Anda.</div>");die();
        }
    }
    
    public function aktifitas_detail_data($param){
        if(empty($param->param->id)){
            return $this->response_gagal("02", "ID pekerjaan tidak tersedia");die();
        }
        $id = $param->param->id;
        $query = "SELECT * FROM pekerjaan_sub where pekerjaan_id = ? order by pekerjaan_sub_id asc";
        $result = $this->db_prod->query($query, $id);
        if($result->num_rows() > 0){
            $response_sub = array();
            foreach($result->result() as $row){
                $response_sub[] = array(
                    "pekerjaan_sub_id" => $row->pekerjaan_sub_id,
                    "pekerjaan_id" => $row->pekerjaan_id,
                    "pekerjaan_sub_nama" => $row->pekerjaan_sub_nama,
                );
            }
            
            $query2 = "SELECT * FROM rumus order by id asc";
            $result2 = $this->db_prod->query($query2);
            if($result2->num_rows() > 0){
                $response_field = array();
                foreach($result2->result() as $row){
                    $response_field[] = array(
                        "id" => $row->id,
                        "kunci" => $row->kunci,
                        "nama" => $row->nama,
                    );
                }
            }
            
            $resp_all = array(
                "data_pekerjaan" => json_encode($response_sub),
                "data_field" => json_encode($response_field),
            );
            
            return $this->response_sukses($resp_all);
        }else{
            return $this->response_gagal_html("02", "Belum ada detail aktifitas, silahkan tambahkan detail aktifitas Anda.");die();
        }
    }
    
    public function aktifitas_detail_data_edit($param){
        if(empty($param->param->id)){
            return $this->response_gagal("02", "ID pekerjaan tidak tersedia");die();
        }
        $id = $param->param->id;
        $query = "SELECT p.pekerjaan_uraian_detail_id, p.kegiatan_id, p.pekerjaan_id, p.pekerjaan_sub_id, p.pejabat_id,
                p.pekerjaan_uraian_id,p.pekerjaan_uraian_detail_keterangan, 
                p.pekerjaan_uraian_detail_rumus, p.pekerjaan_uraian_detail_verificator, p.pekerjaan_uraian_detail_creator,
                p.pekerjaan_uraian_detail_sketsa, p.pekerjaan_uraian_detail_create_at, pekerjaan_uraian_detail_fields,
                pu.pekerjaan_uraian_nama,
                ps.pekerjaan_sub_nama
                FROM pekerjaan_uraian_detail p
                left join pekerjaan_uraian pu on p.pekerjaan_uraian_id = pu.pekerjaan_uraian_id
                left join pekerjaan_sub ps on p.pekerjaan_sub_id = ps.pekerjaan_sub_id
                where p.pekerjaan_uraian_detail_id = ?
                order by p.pekerjaan_uraian_detail_id desc limit 1";
        $result = $this->db_prod->query($query, $id);
        if($result->num_rows() > 0){
            $row = $result->row();
            $response_sub = array(
                "pekerjaan_uraian_detail_id" => $row->pekerjaan_uraian_detail_id,
                "kegiatan_id" => $row->kegiatan_id,
                "pekerjaan_id" => $row->pekerjaan_id,
                "pekerjaan_sub_id" => $row->pekerjaan_sub_id,
                "pekerjaan_sub_nama" => $row->pekerjaan_sub_nama,
                "pejabat_id" => $row->pejabat_id,
                "pekerjaan_uraian_id" => $row->pekerjaan_uraian_id,
                "pekerjaan_uraian_nama" => $row->pekerjaan_uraian_nama,
                "pekerjaan_uraian_detail_keterangan" => $row->pekerjaan_uraian_detail_keterangan,
                "pekerjaan_uraian_detail_rumus" => $row->pekerjaan_uraian_detail_rumus,
                "pekerjaan_uraian_detail_verificator" => $row->pekerjaan_uraian_detail_verificator,
                "pekerjaan_uraian_detail_creator" => $row->pekerjaan_uraian_detail_creator,
                "pekerjaan_uraian_detail_sketsa" => $row->pekerjaan_uraian_detail_sketsa,
                "pekerjaan_uraian_detail_fields" => $row->pekerjaan_uraian_detail_fields,
                "pekerjaan_uraian_detail_create_at" => $row->pekerjaan_uraian_detail_create_at,
                
            );
            return $this->response_sukses($response_sub);
        }else{
            return $this->response_gagal_html("02", "Belum ada detail aktifitas, silahkan tambahkan detail aktifitas Anda.");die();
        }
    }
    
    public function aktifitas_detail_data_uraian($param){
        if(empty($param->param->id)){
            return $this->response_gagal("02", "ID sub pekerjaan tidak tersedia");die();
        }
        $id = $param->param->id;
        $query = "SELECT pekerjaan_uraian_id, pekerjaan_uraian_nama 
                    FROM pekerjaan_uraian 
                    WHERE pekerjaan_sub_id = ? AND pekerjaan_uraian_id NOT IN (
                        SELECT pekerjaan_uraian_id
                        FROM pekerjaan_uraian_detail
                        WHERE pekerjaan_sub_id = ?)";
        $result = $this->db_prod->query($query, array($id, $id));
        if($result->num_rows() > 0){
            $response_sub = array();
            foreach($result->result() as $row){
                $response_sub[] = array(
                    "pekerjaan_uraian_id" => $row->pekerjaan_uraian_id,
                    "pekerjaan_uraian_nama" => $row->pekerjaan_uraian_nama,
                );
            }
            
            $resp_all = array(
                "data_uraian_pekerjaan" => json_encode($response_sub),
            );
            
            return $this->response_sukses($resp_all);
        }else{
            return $this->response_gagal_html("02", "Belum ada detail aktifitas, silahkan tambahkan detail aktifitas Anda.");die();
        }
    }
    
    public function aktifitas_detail_add($param){
        if(empty($param->param->id)){
            return $this->response_gagal("02", "ID pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->nama)){
            return $this->response_gagal("02", "Nama pekerjaan tidak tersedia");die();
        }
       
        $insertdata = array(
            "pekerjaan_id" => $param->param->id,
            "pekerjaan_sub_nama" => strtoupper($param->param->nama),
            "updated_at" => date('Y-m-d H:i:s')
        );
        
        $insert = $this->db_prod->insert("pekerjaan_sub", $insertdata);
        if(!$insert){
             return $this->response_gagal("02", "Proses gagal, silahkan ulangi kembali");
        }else{
            return $this->response_sukses("Proses Tambah Data Berhasil, ID ".$insert);
        }
    }
    
    public function aktifitas_detail_add_data($param){
        if(empty($param->param->pekerjaan_id)){
            return $this->response_gagal("02", "ID pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_sub_id)){
            return $this->response_gagal("02", "ID sub pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_uraian_id)){
            return $this->response_gagal("02", "ID uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_jenis)){
            return $this->response_gagal("02", "Jenis uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_nama_verificator)){
            return $this->response_gagal("02", "Nama verifikator tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_nama_creator)){
            return $this->response_gagal("02", "Nama creator tidak tersedia");die();
        }
        
        $query = "SELECT p.kontraktor_id, p.kegiatan_id, p.pekerjaan_id,
            pj.pejabat_id
            FROM pekerjaan p
            left join pekerjaan_pejabat pj on p.pekerjaan_id = pj.pekerjaan_id
            where p.pekerjaan_id = ? limit 1";
        $result = $this->db_prod->query($query, array($param->param->pekerjaan_id));
        $kegiatan_id = "";
        $pejabat_id = "";
        if($result->num_rows() > 0){
            $rst = $result->row();
            $kegiatan_id = $rst->kegiatan_id;
            $pejabat_id = $rst->pejabat_id;
        }
        
        $insertdata = array(
            "kegiatan_id" => $kegiatan_id,
            "pekerjaan_id" => $param->param->pekerjaan_id,
            "pekerjaan_sub_id" => $param->param->pekerjaan_sub_id,
            "pejabat_id" => $pejabat_id,
            "pekerjaan_uraian_id" => $param->param->pekerjaan_uraian_id,
            "pekerjaan_uraian_detail_fields" => $param->param->pekerjaan_jenis,
            "pekerjaan_uraian_detail_verificator" => $param->param->pekerjaan_nama_verificator,
            "pekerjaan_uraian_detail_creator" => $param->param->pekerjaan_nama_creator,
            "pekerjaan_uraian_detail_create_at" => date('Y-m-d H:i:s'),
            
        );
        $insert = $this->db_prod->insert("pekerjaan_uraian_detail", $insertdata);
        if(!$insert){
             return $this->response_gagal("02", "Proses gagal, silahkan ulangi kembali");
        }else{
            return $this->response_sukses("Proses Tambah Data Uraian Pekerjaan Berhasil, ID ".$insert);
        }
    }
    
    public function aktifitas_detail_edit_data($param){
        if(empty($param->param->pekerjaan_id)){
            return $this->response_gagal("02", "ID pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_uraian_detail_id)){
            return $this->response_gagal("02", "ID uraian detail pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_sub_id)){
            return $this->response_gagal("02", "ID sub pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_uraian_id)){
            return $this->response_gagal("02", "ID uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_jenis)){
            return $this->response_gagal("02", "Jenis uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_nama_verificator)){
            return $this->response_gagal("02", "Nama verifikator tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_nama_creator)){
            return $this->response_gagal("02", "Nama creator tidak tersedia");die();
        }
        
        $update = array(
            "pekerjaan_sub_id" => $param->param->pekerjaan_sub_id,
            "pekerjaan_uraian_id" => $param->param->pekerjaan_uraian_id,
            "pekerjaan_uraian_detail_fields" => $param->param->pekerjaan_jenis,
            "pekerjaan_uraian_detail_verificator" => $param->param->pekerjaan_nama_verificator,
            "pekerjaan_uraian_detail_creator" => $param->param->pekerjaan_nama_creator,
        );
        $update = $this->processing_update_data("pekerjaan_uraian_detail", "pekerjaan_uraian_detail_id", $param->param->pekerjaan_uraian_detail_id, $update);
        if(!$update){
             return $this->response_gagal("02", "Proses gagal, silahkan ulangi kembali");
        }else{
            return $this->response_sukses("Proses Edit Data Berhasil");
        }
    }

    
//    public function aktifitas_detail_edit($param){
//        if(empty($param->param->id)){
//            return $this->response_gagal("02", "ID Sub pekerjaan tidak tersedia");die();
//        }
//        if(empty($param->param->nama)){
//            return $this->response_gagal("02", "Nama pekerjaan tidak tersedia");die();
//        }
//      
//        $updatedata = array(
//            "pekerjaan_sub_nama" => strtoupper($param->param->nama),
//            "updated_at" => date('Y-m-d H:i:s')
//        );
//        
//        $update = $this->processing_update_data("pekerjaan_sub", "pekerjaan_sub_id", $param->param->id, $updatedata);
//        if(!$update){
//             return $this->response_gagal("02", "Proses gagal, silahkan ulangi kembali");
//        }else{
//            return $this->response_sukses("Proses Edit Data Berhasil");
//        }
//    }
    
    public function aktifitas_detail_sub($param){
        if(empty($param->param->kegiatan_id)){
            return $this->response_gagal("02", "ID kegiatan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_id)){
            return $this->response_gagal("02", "ID pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_sub_id)){
            return $this->response_gagal("02", "ID sub pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_uraian_id)){
            return $this->response_gagal("02", "ID uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_uraian_detail_id)){
            return $this->response_gagal("02", "ID detail uraian pekerjaan tidak tersedia");die();
        }
        
        $data_kegiatan_id = $param->param->kegiatan_id;
        $data_pekerjaan_id = $param->param->pekerjaan_id;
        $data_pekerjaan_sub_id = $param->param->pekerjaan_sub_id;
        $data_pekerjaan_uraian_id = $param->param->pekerjaan_uraian_id;
        $data_pekerjaan_uraian_detail_id = $param->param->pekerjaan_uraian_detail_id;
        
        // cek data
        $qry_cek = "SELECT pekerjaan_uraian_detail_fields FROM pekerjaan_uraian_detail 
                where pekerjaan_uraian_detail_id = ?";
        
        $result_cek = $this->db_prod->query($qry_cek, $data_pekerjaan_uraian_detail_id);
        $data_field = "";
        if($result_cek->num_rows() > 0){
            $result_row = $result_cek->row();
            $data_field = $result_row->pekerjaan_uraian_detail_fields;
        }
        
        // parse data field
        $parse_data_field = explode(";", $data_field);
        $volume_data = $parse_data_field[count($parse_data_field) - 1];
        
        $html_attr = "";
        $html_attr .= "<tr>";
        foreach($parse_data_field as $row){
            $html_attr .= "<td><center>".$row."</center></td>";
        }
        $html_attr .= "</tr>";
        
        $collection = "uraian_".$data_pekerjaan_sub_id."_".$data_pekerjaan_uraian_id;
        $key = "detail_".$data_kegiatan_id."_".$data_pekerjaan_id."_".$data_pekerjaan_sub_id."_".$data_pekerjaan_uraian_id."_".$data_pekerjaan_uraian_detail_id;
        
        $arr = array(
            "collection" => $collection,
            "data" => array(
                "key" => $key
            )
        );
        $response = $this->sendPostData($arr, "search-detail");
        
//        $response = '{"data":[{"_id":"5cee3a36853337fdc9f0d9d0","detail":{"aktifitas":[{"2018-10-10":{"gambar":"https://tabalong.kalsel.polri.go.id/wp-content/uploads/2017/09/IMG_20170718_114921-e1505642091539.jpg","proses":30}}],"jumlah":1,"lebar":4.8,"panjang":6,"tinggi":2.5,"vol.(m3)":72},"keterangan":"Pondasi Utara","key":"detail_1_1_7_19_6"},{"_id":"5cee3a92853337fdc9f0d9d3","detail":{"aktifitas":[{"2018-10-10":{"gambar":"https://www.performancesuretybonds.com/blog/wp-content/uploads/2018/12/bigstock-212076514-1024x684.jpg","proses":50.508}},{"2018-10-23":{"gambar":"https://tabalong.kalsel.polri.go.id/wp-content/uploads/2017/09/IMG_20170718_114921-e1505642091539.jpg","proses":75.76}}],"jumlah":1,"lebar":3.05,"panjang":6,"tinggi":6.9,"vol.(m3)":126.27},"keterangan":"Pondasi Utara (atas)","key":"detail_1_1_7_19_6"},{"_id":"5cee3b6b853337fdc9f0d9d5","detail":{"aktifitas":[{"2018-10-06":{"gambar":"https://www.performancesuretybonds.com/blog/wp-content/uploads/2018/12/bigstock-212076514-1024x684.jpg","proses":43.2}}],"jumlah":1,"lebar":3.6,"panjang":6,"tinggi":2,"vol.(m3)":43.2},"keterangan":"Pondasi Selatan","key":"detail_1_1_7_19_6"},{"_id":"5cee3bd4853337fdc9f0d9d8","detail":{"aktifitas":[{"2018-10-12":{"gambar":"https://tabalong.kalsel.polri.go.id/wp-content/uploads/2017/09/IMG_20170718_114921-e1505642091539.jpg","proses":46.58}},{"2018-10-20":{"gambar":"https://www.performancesuretybonds.com/blog/wp-content/uploads/2018/12/bigstock-212076514-1024x684.jpg","proses":46.58}}],"jumlah":1,"lebar":2.25,"panjang":6,"tinggi":6.9,"vol.(m3)":93.15},"keterangan":"Pondasi Selatan (atas)","key":"detail_1_1_7_19_6"},{"_id":"5cee4234853337fdc9f0d9da","detail":{"aktifitas":[{"2018-10-02":{"gambar":null,"proses":95}}],"jumlah":1,"lebar":3.8,"panjang":10,"tinggi":2.5,"vol.(m3)":95},"keterangan":"Pilar Tengah (pondasi)","key":"detail_1_1_7_19_6"},{"_id":"5cee42a1853337fdc9f0d9dd","detail":{"aktifitas":[{"2018-10-16":{"gambar":null,"proses":51.75}},{"2018-10-25":{"gambar":null,"proses":51.75}}],"jumlah":1,"lebar":2.5,"panjang":6,"tinggi":6.9,"vol.(m3)":103.5},"keterangan":"Pilar Tengah (atas)","key":"detail_1_1_7_19_6"},{"_id":"5cee4497853337fdc9f0d9e0","detail":{"aktifitas":[{"2018-10-17":{"gambar":null,"proses":18.30777}},{"2018-10-27":{"gambar":null,"proses":18.31}}],"jumlah":1,"lebar":1.3,"panjang":3.14,"tinggi":6.9,"vol.(m3)":36.62},"keterangan":"Pilar Tengah (tepi)","key":"detail_1_1_7_19_6"},{"_id":"5cee450c853337fdc9f0d9e2","detail":{"aktifitas":[{"2018-11-06":{"gambar":null,"proses":10.5}}],"jumlah":1,"lebar":0.525,"panjang":10,"tinggi":2,"vol.(m3)":10.5},"keterangan":"Pot. B","key":"detail_1_1_7_19_6"},{"_id":"5cee4546853337fdc9f0d9e4","detail":{"aktifitas":[{"2018-11-06":{"gambar":null,"proses":9.45}}],"jumlah":1,"lebar":0.525,"panjang":9,"tinggi":2,"vol.(m3)":9.45},"keterangan":"Pot. D","key":"detail_1_1_7_19_6"},{"_id":"5cee4752853337fdc9f0d9e6","detail":{"aktifitas":[{"2018-10-26":{"gambar":null,"proses":19.56}}],"jumlah":2,"lebar":1.35,"panjang":1.05,"tinggi":6.9,"vol.(m3)":19.56},"keterangan":"Pot. E - E (dalam)","key":"detail_1_1_7_19_6"},{"_id":"5cee4956853337fdc9f0d9e9","detail":{"aktifitas":[{"2018-10-26":{"gambar":null,"proses":6.6}}],"jumlah":2,"lebar":0.6,"panjang":5,"tinggi":1.1,"vol.(m3)":6.6},"keterangan":"Pot. E - E (tengah)","key":"detail_1_1_7_19_6"},{"_id":"5cee4997853337fdc9f0d9eb","detail":{"aktifitas":[{"2018-10-26":{"gambar":null,"proses":45.6}}],"jumlah":2,"lebar":1,"panjang":2.85,"tinggi":8,"vol.(m3)":45.6},"keterangan":"Pot. E - E (luar)","key":"detail_1_1_7_19_6"},{"_id":"5cee4a14853337fdc9f0d9ef","detail":{"aktifitas":[{"2018-10-26":{"gambar":null,"proses":48.6}},{"2018-11-17":{"gambar":null,"proses":8.1}},{"2018-12-02":{"gambar":null,"proses":8.1}}],"jumlah":2,"lebar":1.2,"panjang":20,"tinggi":1.35,"vol.(m3)":64.8},"keterangan":"Pot. F - G (pondasi)","key":"detail_1_1_7_19_6"},{"_id":"5cee4aab853337fdc9f0d9f3","detail":{"aktifitas":[{"2018-10-20":{"gambar":null,"proses":126}},{"2018-10-26":{"gambar":null,"proses":21}},{"2018-12-02":{"gambar":null,"proses":21}}],"jumlah":2,"lebar":1.2,"panjang":20,"tinggi":3.5,"vol.(m3)":168},"keterangan":"Pot. F - G (atas)","key":"detail_1_1_7_19_6"},{"_id":"5cee4c57853337fdc9f0d9f6","detail":{"aktifitas":[{"2018-10-16":{"gambar":null,"proses":28.41075}},{"2018-10-26":{"gambar":null,"proses":28.41}}],"jumlah":2,"lebar":1.35,"panjang":3.05,"tinggi":6.9,"vol.(m3)":56.82},"keterangan":"Pot. I - I (pondasi)","key":"detail_1_1_7_19_6"},{"_id":"5cee4cb4853337fdc9f0d9f8","detail":{"aktifitas":[{"2018-10-26":{"gambar":null,"proses":6.6}}],"jumlah":2,"lebar":0.6,"panjang":5,"tinggi":1.1,"vol.(m3)":6.6},"keterangan":"Pot. I - I (atas)","key":"detail_1_1_7_19_6"},{"_id":"5cee4cf8853337fdc9f0d9fa","detail":{"aktifitas":[{"2018-11-24":{"gambar":null,"proses":5}}],"jumlah":4,"lebar":0.5,"panjang":5,"tinggi":1,"vol.(m3)":10},"keterangan":"Bok","key":"detail_1_1_7_19_6"}],"rc":"00","rd":"Sukses"}';
//        $response = '{"data":[{"_id":"5ced215e853337fdc9f0d993","detail":{"aktifitas":[{"2018-11-14":{"gambar":null,"proses":118.8}}],"jumlah":11,"keliling":1.8,"panjang":6,"vol.(m2)":118.8},"keterangan":"WF 500x200x10x16 (bagian 2)","key":"detail_1_1_8_27_2"},{"_id":"5ced23a4853337fdc9f0d995","detail":{"aktifitas":[{"2018-11-20":{"gambar":null,"proses":35}}],"jumlah":70,"keliling":1,"panjang":0.5,"vol.(m2)":35},"keterangan":"Canal UNP 300x100x10","key":"detail_1_1_8_27_2"},{"_id":"5ced23f1853337fdc9f0d997","detail":{"aktifitas":[{"2018-11-13":{"gambar":null,"proses":15}}],"jumlah":30,"keliling":1,"panjang":0.5,"vol.(m2)":15},"keterangan":"Canal UNP 300x100x10 (bagian 2)","key":"detail_1_1_8_27_2"},{"_id":"5ced24f3853337fdc9f0d999","detail":{"aktifitas":[{"2018-11-22":{"gambar":null,"proses":17.6}}],"jumlah":44,"keliling":0.5,"panjang":0.8,"vol.(m2)":17.6},"keterangan":"Plat sambungan t = 1 cm (samping)","key":"detail_1_1_8_27_2"},{"_id":"5ced254f853337fdc9f0d99b","detail":{"aktifitas":[{"2018-11-22":{"gambar":null,"proses":9.68}}],"jumlah":44,"keliling":0.2,"panjang":1.1,"vol.(m2)":9.68},"keterangan":"Plat sambungan t = 2 cm (dalam atas)","key":"detail_1_1_8_27_2"},{"_id":"5ced2577853337fdc9f0d99d","detail":{"aktifitas":[{"2018-11-22":{"gambar":null,"proses":9.68}}],"jumlah":44,"keliling":0.2,"panjang":1.1,"vol.(m2)":9.68},"keterangan":"Plat sambungan t = 2 cm (dalam bawah)","key":"detail_1_1_8_27_2"},{"_id":"5ced25ba853337fdc9f0d99f","detail":{"aktifitas":[{"2018-11-22":{"gambar":null,"proses":8.36}}],"jumlah":22,"keliling":0.2,"panjang":1.9,"vol.(m2)":8.36},"keterangan":"Plat sambungan t = 2 cm (luar bawah)","key":"detail_1_1_8_27_2"},{"_id":"5ced2601853337fdc9f0d9a1","detail":{"aktifitas":[{"2018-11-22":{"gambar":null,"proses":35.2}}],"jumlah":11,"keliling":0.2,"panjang":16,"vol.(m2)":35.2},"keterangan":"Cover Plat sambungan t = 2 cm","key":"detail_1_1_8_27_2"},{"_id":"5ced263d853337fdc9f0d9a3","detail":{"aktifitas":[{"2018-11-22":{"gambar":null,"proses":15.4}}],"jumlah":154,"keliling":0.2,"panjang":0.5,"vol.(m2)":15.4},"keterangan":"Lateral stiffener t = 1 cm","key":"detail_1_1_8_27_2"},{"_id":"5ced2676853337fdc9f0d9a5","detail":{"aktifitas":[{"2018-11-22":{"gambar":null,"proses":6.6}}],"jumlah":66,"keliling":0.2,"panjang":0.5,"vol.(m2)":6.6},"keterangan":"Lateral stiffener t = 1 cm (bagian 2)","key":"detail_1_1_8_27_2"},{"_id":"5d119439853337fdc9f0d9fc","detail":{"aktifitas":[{"2018-11-24":{"gambar":null,"proses":396}},{"2019-06-25":{"gambar":"http://admin-pu.jatimsekawanhati.com/uploads/client/newimage_1631515058.jpg","proses":-198}}],"jumlah":11,"keliling":1.8,"panjang":20,"vol.(m2)":396},"keterangan":"WF 500x200x10x16","key":"detail_1_1_8_27_2"}],"rc":"00","rd":"Sukses"}';
        
        $response = json_decode($response);
        if(!empty($response) && $response->rc == "00" && $response->data != null){
            // get data from db
            $resp_table = array();
            $qry_akt = "SELECT * FROM laporan_aktifitas where pekerjaan_uraian_id = ? and status = 0 order by created_at asc";
            $result_akt = $this->db_prod->query($qry_akt, $data_pekerjaan_uraian_id);
            if($result_akt->num_rows() > 0){
                foreach($result_akt->result() as $row_akt){
                    $resp_table[] = array(
                        "pekerjaan_uraian_id" => $row_akt->pekerjaan_uraian_id,
                        "collection_mongo" => $row_akt->collection_mongo,
                        "key_mongo" => $row_akt->key_mongo,
                        "ket_mongo" => $row_akt->ket_mongo,
                        "tanggal" => $row_akt->tanggal,
                        "gambar" => $row_akt->gambar,
                        "proses" => $row_akt->proses,
                        "status" => $row_akt->status,
                        "longitude" => $row_akt->longitude,
                        "latitude" => $row_akt->latitude,
                        "created_at" => $row_akt->created_at,
                        "tanggal_approve" => $row_akt->tanggal_approve,
                    );
                }
            }
            
            $resp = array();
            $no = 1;
            $html = "";
            foreach($response->data as $value => $row){
                $attribute = "";
                if(in_array($value, $parse_data_field)){
                    $attribute = $parse_data_field[$value];
                }
                
                $resp_detail = array();
                $volume = $row->detail->$volume_data;
                $akt = "";
                $html_attr_value = "";
                foreach ($row->detail->aktifitas as $item){
                    $tanggal = "";
                    $gambar = "";
                    $proses = "";
                    foreach($item as $key => $val) {
                        $tanggal = $key;
                        $gambar = $val->gambar;
                        $proses = $val->proses;
                    }
                    
                    if($gambar == "" || $gambar == null){
                        $gambar = 'http://api-pu.jatimsekawanhati.com/assets/images/img-coming-soon.png';
                    }
                    
                    $resp_detail[] = array(
                        "tanggal" => $this->format_date_indo($tanggal),
                        "image" => $gambar,
                        "proses" => $proses,
                        "status" => 1,
                    );
                }
                // gabungkan dengan data dari tabel
                foreach($resp_table as $row_akt){
                    if($row->key == $row_akt['key_mongo'] && strtolower($row->keterangan) == strtolower($row_akt['ket_mongo'])){
                        $resp_detail[] = array(
                            "tanggal" => $this->format_date_indo($row_akt['tanggal']),
                            "image" => $row_akt['gambar'],
                            "proses" => $row_akt['proses'],
                            "status" => 0
                        );
                    }
                }
                arsort($resp_detail);
                
                // create html
                $prosentase = 0;
                $proses = 0;
                
                $akt .= "<div class='col-md-12 col-xs-12' style='margin-bottom:5px;'>
                            <div class='owl-carousel owl-theme'>
                        ";
                foreach($resp_detail as $row_detail_img){
                    $proses += $row_detail_img['proses'];
                    $proses_data = $row_detail_img['proses'];
                    $prosentase_sementara = 0;
                    $prosentase_sementara = intval($proses_data) / intval($volume) * 100;
                    if(intval($prosentase_sementara) < 100){
                        $prosentase_sementara = number_format($prosentase_sementara, 2);
                    }
                    
                    $parse_img = str_replace("/", "--", $row_detail_img['image']);
                    $status = "Belum Diverifikasi";
                    if($row_detail_img['status'] == 0){
                        $akt .= "<div class='item'>
                                <a href='#/laporan_aktifitas_detail_aktifitas_sub_img/".$parse_img."/$status/$prosentase_sementara'>
                                    <img src='".$row_detail_img['image']."' style='width: 100%; height:120px;overflow: hidden;-webkit-filter: grayscale(100%);filter: grayscale(100%);-webkit-filter: blur(3px);filter: blur(3px);'>
                                </a>
                            </div>";
                    }else{
                        $status = "Terverifikasi";
                        $akt .= "<div class='item'>
                                <a href='#/laporan_aktifitas_detail_aktifitas_sub_img/".$parse_img."/$status/$prosentase_sementara'>
                                    <img src='".$row_detail_img['image']."' style='width: 100%; height:120px;overflow: hidden;'>
                                </a>
                            </div>";
                    }
                    
                }
                $akt .= "</div></div>";
                
                //$proses = 3;
                $prosentase = intval($proses) / intval($volume) * 100;
                $sisa_prosentase = intval($volume) - intval($proses);
                $sisa_prosentase_persen = intval($sisa_prosentase) / intval($volume) * 100;
                
                $html_attr_value .= "<tr>";
                foreach($parse_data_field as $row_field){
                    $html_attr_value .= "<td><center>".$row->detail->$row_field."</center></td>";
                }
                $html_attr_value .= "</tr>";
                
                $html .= "<div class='col-md-12 col-xs-12' style='margin-bottom:5px;'>
                                <p class='title-pekerjaan'><b>".$no.". ".ucwords(strtolower($row->keterangan))."</b>
                                </p>
                            </div>
                            <div class='col-md-12 col-xs-12' style='margin-top: 0px; margin-bottom:5px;'>
                                <b><span class='fa fa-1x fa-graduation-cap'></span>&nbsp;</b>Pencapaian : ".  $prosentase."%
                                <a href='#/laporan_aktifitas_detail_aktifitas_sub_edit/$data_kegiatan_id/$data_pekerjaan_id/$data_pekerjaan_sub_id/$data_pekerjaan_uraian_id/$data_pekerjaan_uraian_detail_id/$row->keterangan/$sisa_prosentase_persen/$data_field' style='color: #333;' class='pull-right'>
                                    <span class='fa fa-1x fa-pencil'></span>
                                    <i style='font-style: normal; font-size: 12px'>Edit Tabel</i>
                                </a>
                            </div>
                            <div class='col-md-12 col-xs-12'>
                                <table class='table table-bordered'>
                                    ".$html_attr.$html_attr_value."
                                </table>
                            </div>
                            ".$akt;
                                
                if(intval($prosentase) < 100){
                    $html .= "<div class='col-md-12 col-xs-12' style='margin-bottom:10px; margin-top:5px;'>
                                    <a href='#/laporan_aktifitas_detail_aktifitas_sub_status/$data_kegiatan_id/$data_pekerjaan_id/$data_pekerjaan_sub_id/$data_pekerjaan_uraian_id/$data_pekerjaan_uraian_detail_id/$row->keterangan/$sisa_prosentase_persen/$volume' class='btn btn-block btn-primary btn-flat'><span class='fa fa-1x fa-camera'></span>&nbsp;Update status</a>
                                </div>";
                }   
                
                $html .= "<div class='col-md-12 col-xs-12'>
                                <hr style='margin-top:5px; margin-bottom:7px; color:#333;border-top:1px solid #ccc'>
                            </div>
                            ";
                $no++;
            }
            return $this->response_sukses("", $html);
        }else{
            return $this->response_gagal_html("02", "<div class='col-md-12 col-xs-12'>Detail uraian aktifitas tidak tersedia, silahkan di tambahkan</div>");
        }
    }
    
    public function aktifitas_detail_sub_edit($param){
        if(empty($param->param->kegiatan_id)){
            return $this->response_gagal("02", "ID kegiatan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_id)){
            return $this->response_gagal("02", "ID pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_sub_id)){
            return $this->response_gagal("02", "ID sub pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_uraian_id)){
            return $this->response_gagal("02", "ID uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_uraian_detail_id)){
            return $this->response_gagal("02", "ID detail uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->keterangan)){
            return $this->response_gagal("02", "Keterangan uraian aktifitas tidak tersedia");die();
        }
        if(empty($param->param->field)){
            return $this->response_gagal("02", "Field uraian aktifitas tidak tersedia");die();
        }
        
        $data_kegiatan_id = $param->param->kegiatan_id;
        $data_pekerjaan_id = $param->param->pekerjaan_id;
        $data_pekerjaan_sub_id = $param->param->pekerjaan_sub_id;
        $data_pekerjaan_uraian_id = $param->param->pekerjaan_uraian_id;
        $data_pekerjaan_uraian_detail_id = $param->param->pekerjaan_uraian_detail_id;
        $data_keterangan = $param->param->keterangan;
        $data_field = explode(";", $param->param->field);
        
        //sebelum di insert, diupdate all dulu
        $collection = "uraian_".$data_pekerjaan_sub_id."_".$data_pekerjaan_uraian_id;
        $key = "detail_".$data_kegiatan_id."_".$data_pekerjaan_id."_".$data_pekerjaan_sub_id."_".$data_pekerjaan_uraian_id."_".$data_pekerjaan_uraian_detail_id;
        
        $arr = array(
            "collection" => $collection,
            "data" => array(
                "key" => $key,
                "keterangan" => $data_keterangan
            )
        );
        
        $response = $this->sendPostData($arr, "search-detail");
        $response = json_decode($response);
        if(!empty($response) && $response->rc == "00" && $response->data != null){
            $html = "";
            foreach($response->data as $value => $row){
                foreach($data_field as $row_field){
                    $new_data_field = str_replace(array(".","(",")",",","#"), "", $row_field);
                    
                    $html .= "<div class='form-group'>
                                <label style='font-weight:600'>". ucwords(strtolower($row_field))."</label><br>
                                <input type='number' class='form-control form-lg' name='$new_data_field' id='$new_data_field' ng-model='$new_data_field' placeholder='Masukkan $row_field' value='".$row->detail->$row_field."'>
                            </div>";
                }
            }
            return $this->response_sukses("", $html);
        }else{
            return $this->response_gagal("02", "Proses gagal, silahkan ulangi kembali");die();
        }
    }
    
    public function aktifitas_detail_sub_edit_save($param){
        if(empty($param->param->kegiatan_id)){
            return $this->response_gagal("02", "ID kegiatan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_id)){
            return $this->response_gagal("02", "ID pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_sub_id)){
            return $this->response_gagal("02", "ID sub pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_uraian_id)){
            return $this->response_gagal("02", "ID uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_uraian_detail_id)){
            return $this->response_gagal("02", "ID detail uraian aktifitas tidak tersedia");die();
        }
        if(empty($param->param->keterangan)){
            return $this->response_gagal("02", "Keterangan uraian aktifitas tidak tersedia");die();
        }
        if(empty($param->param->field)){
            return $this->response_gagal("02", "Field uraian aktifitas tidak tersedia");die();
        }
        if(empty($param->param->value)){
            return $this->response_gagal("02", "Value uraian aktifitas tidak tersedia");die();
        }
        
        $data_kegiatan_id = $param->param->kegiatan_id;
        $data_pekerjaan_id = $param->param->pekerjaan_id;
        $data_pekerjaan_sub_id = $param->param->pekerjaan_sub_id;
        $data_pekerjaan_uraian_id = $param->param->pekerjaan_uraian_id;
        $data_pekerjaan_uraian_detail_id = $param->param->pekerjaan_uraian_detail_id;
        $data_keterangan = $param->param->keterangan;
        $data_field = $param->param->field;
        $data_value = $param->param->value;
        
        //sebelum di insert, diupdate all dulu
        $collection = "uraian_".$data_pekerjaan_sub_id."_".$data_pekerjaan_uraian_id;
        $key = "detail_".$data_kegiatan_id."_".$data_pekerjaan_id."_".$data_pekerjaan_sub_id."_".$data_pekerjaan_uraian_id."_".$data_pekerjaan_uraian_detail_id;
        
        $arr = array(
            "collection" => $collection,
            "data" => array(
                "key" => $key,
                "keterangan" => $data_keterangan,
                "field" => $data_field,
                "value" => $data_value
            )
        );
        $response = json_decode($this->sendPostData($arr, "update-detail"));
        if(!empty($response) && $response->rc == "00"){
            return $this->response_sukses("sukses");
        }else{
            return $this->response_gagal("02", "Proses gagal, silahkan ulangi kembali");die();
        }
    }
    
    public function aktifitas_detail_sub_add($param){
        if(empty($param->param->kegiatan_id)){
            return $this->response_gagal("02", "ID kegiatan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_id)){
            return $this->response_gagal("02", "ID pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_sub_id)){
            return $this->response_gagal("02", "ID sub pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_uraian_id)){
            return $this->response_gagal("02", "ID uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_uraian_detail_id)){
            return $this->response_gagal("02", "ID detail uraian pekerjaan tidak tersedia");die();
        }
        
        $data_kegiatan_id = $param->param->kegiatan_id;
        $data_pekerjaan_id = $param->param->pekerjaan_id;
        $data_pekerjaan_sub_id = $param->param->pekerjaan_sub_id;
        $data_pekerjaan_uraian_id = $param->param->pekerjaan_uraian_id;
        $data_pekerjaan_uraian_detail_id = $param->param->pekerjaan_uraian_detail_id;
        
        // cek data
        $qry_cek = "SELECT pekerjaan_uraian_detail_fields FROM pekerjaan_uraian_detail 
                where pekerjaan_uraian_detail_id = ?";
        
        $result_cek = $this->db_prod->query($qry_cek, $data_pekerjaan_uraian_detail_id);
        $data_field = "";
        if($result_cek->num_rows() > 0){
            $result_row = $result_cek->row();
            $data_field = $result_row->pekerjaan_uraian_detail_fields;
        }
        
        // parse data field
        $parse_data_field = explode(";", $data_field);
        
        $html = "";
        foreach($parse_data_field as $row_field){
            $new_data_field = str_replace(array(".","(",")",",","#"), "", $row_field);
            
            $html .= "<div class='form-group'>
                        <input type='number' class='form-control form-lg' name='$new_data_field' id='$new_data_field' ng-model='$new_data_field' placeholder='Masukkan $row_field'>
                    </div>";
        }
        return $this->response_sukses($data_field, $html);
    }
    
    public function aktifitas_detail_sub_add_save($param){
        if(empty($param->param->kegiatan_id)){
            return $this->response_gagal("02", "ID kegiatan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_id)){
            return $this->response_gagal("02", "ID pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_sub_id)){
            return $this->response_gagal("02", "ID sub pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_uraian_id)){
            return $this->response_gagal("02", "ID uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_uraian_detail_id)){
            return $this->response_gagal("02", "ID detail uraian aktifitas tidak tersedia");die();
        }
        if(empty($param->param->keterangan)){
            return $this->response_gagal("02", "Keterangan uraian aktifitas tidak tersedia");die();
        }
        if(empty($param->param->field)){
            return $this->response_gagal("02", "Field uraian aktifitas tidak tersedia");die();
        }
        if(empty($param->param->value)){
            return $this->response_gagal("02", "Value uraian aktifitas tidak tersedia");die();
        }
        
        $data_kegiatan_id = $param->param->kegiatan_id;
        $data_pekerjaan_id = $param->param->pekerjaan_id;
        $data_pekerjaan_sub_id = $param->param->pekerjaan_sub_id;
        $data_pekerjaan_uraian_id = $param->param->pekerjaan_uraian_id;
        $data_pekerjaan_uraian_detail_id = $param->param->pekerjaan_uraian_detail_id;
        $data_keterangan = $param->param->keterangan;
        $data_field = $param->param->field;
        $data_value = $param->param->value;
        
        // create json value
        $parse_field = explode(";", $data_field);
        $parse_value = explode(";", $data_value);
        $combine_data = array_combine($parse_field,$parse_value);
        
        $collection = "uraian_".$data_pekerjaan_sub_id."_".$data_pekerjaan_uraian_id;
        $key = "detail_".$data_kegiatan_id."_".$data_pekerjaan_id."_".$data_pekerjaan_sub_id."_".$data_pekerjaan_uraian_id."_".$data_pekerjaan_uraian_detail_id;
        
        $arr = array(
            "collection" => $collection,
            "data" => array(
                "key" => $key,
                "keterangan" => $data_keterangan,
                "detail" => $combine_data
            )
        );
        $response = json_decode($this->sendPostData($arr, "insert-detail"));
        if(!empty($response) && $response->rc == "00"){
            return $this->response_sukses("sukses");
        }else{
            return $this->response_gagal("02", "Proses gagal, silahkan ulangi kembali");die();
        }
    }
    
    public function aktifitas_detail_sub_status($param){
        if(empty($param->param->kegiatan_id)){
            return $this->response_gagal("02", "ID kegiatan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_id)){
            return $this->response_gagal("02", "ID pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_sub_id)){
            return $this->response_gagal("02", "ID sub pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_uraian_id)){
            return $this->response_gagal("02", "ID uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->pekerjaan_uraian_detail_id)){
            return $this->response_gagal("02", "ID detail uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->keterangan)){
            return $this->response_gagal("02", "Keterangan detail uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->progress)){
            return $this->response_gagal("02", "Progress Pencapaian detail uraian pekerjaan tidak tersedia");die();
        }
        if(empty($param->param->image)){
            return $this->response_gagal("02", "Image detail uraian pekerjaan tidak tersedia");die();
        }
        
        $data_kegiatan_id = $param->param->kegiatan_id;
        $data_pekerjaan_id = $param->param->pekerjaan_id;
        $data_pekerjaan_sub_id = $param->param->pekerjaan_sub_id;
        $data_pekerjaan_uraian_id = $param->param->pekerjaan_uraian_id;
        $data_pekerjaan_uraian_detail_id = $param->param->pekerjaan_uraian_detail_id;
        $data_keterangan = $param->param->keterangan;
        $data_progress = $param->param->progress;
        $data_image = $param->param->image;
        $data_volume = $param->param->volume;
        $data_long = $param->param->longitude;
        $data_lat = $param->param->latitude;
        
        $pencapaian = intval($data_progress) / 100 * intval($data_volume);
        
        $collection = "uraian_".$data_pekerjaan_sub_id."_".$data_pekerjaan_uraian_id;
        $key = "detail_".$data_kegiatan_id."_".$data_pekerjaan_id."_".$data_pekerjaan_sub_id."_".$data_pekerjaan_uraian_id."_".$data_pekerjaan_uraian_detail_id;

        // insert ke tabel verifikasi
        $insertdata = array(
            "pekerjaan_uraian_id" => $data_pekerjaan_uraian_id,
            "collection_mongo" => $collection,
            "key_mongo" => $key,
            "ket_mongo" => $data_keterangan,
            "tanggal" => date("Y-m-d H:i:s"),
            "gambar" => $data_image,
            "proses" => $pencapaian,
            "status" => 0,
            "longitude" => $data_long,
            "latitude" => $data_lat,
            "created_at" => date("Y-m-d H:i:s"),
        );
        
        $insert = $this->db_prod->insert("laporan_aktifitas", $insertdata);
        if(!$insert){
            return $this->response_gagal("02", "Proses gagal, silahkan ulangi kembali");
        }else{
            return $this->response_sukses("Proses Tambah Data Berhasil, ID ".$insert.". Silahkan menghubungi pengawas Anda, untuk melakukan proses verifikasi");
        }
        
        //sebelum di insert, diupdate all dulu
//        $collection = "uraian_".$data_pekerjaan_sub_id."_".$data_pekerjaan_uraian_id;
//        $key = "detail_".$data_kegiatan_id."_".$data_pekerjaan_id."_".$data_pekerjaan_sub_id."_".$data_pekerjaan_uraian_id."_".$data_pekerjaan_uraian_detail_id;
//        
//        $arr = array(
//            "collection" => $collection,
//            "data" => array(
//                "key" => $key,
//                "keterangan" => $data_keterangan
//            )
//        );
//        
//        $response = $this->sendPostData($arr, "search-detail");
//        $response = json_decode($response);
//        if(!empty($response) && $response->rc == "00" && $response->data != null){
//            foreach($response->data as $value => $row){
//                $resp_detail = array();
//                foreach ($row->detail->aktifitas as $item) {
//                    $tanggal = "";
//                    $gambar = "";
//                    $proses = "";
//                    foreach($item as $key_data => $val) {
//                        $tanggal = $key_data;
//                        $gambar = $val->gambar;
//                        $proses = $val->proses;
//                    }
//                    
//                    if($gambar == "" || $gambar == null){
//                        $gambar = 'http://api-pu.jatimsekawanhati.com/assets/images/img-coming-soon.png';
//                    }
//                    
//                    $resp_detail[] = array(
//                        "tanggal" => $tanggal,
//                        "image" => $gambar,
//                        "proses" => $proses,
//                    );
//                }
//            }
//            $jml = count($resp_detail) + 1;
//            $bagi_persen = intval($data_progress) / intval($jml);
////            $bagi_persen = floor($bagi_persen);
//            
//            foreach($resp_detail as $row_update){
//                $param_update = array(
//                    "collection" => $collection,
//                    "data" => array(
//                        "key" => $key,
//                        "keterangan" => $data_keterangan,
//                        "aktifitas" => array(
//                            "tanggal" => $row_update['tanggal'],
//                            "field" => "proses",
//                            "value" => $bagi_persen,
//                        ),
//                    ),
//                );
//                $response = json_decode($this->sendPostData($param_update, "update-aktifitas"));
//                if(!empty($response) && $response->rc != "00"){
//                    return $this->response_gagal("02", "Maaf, Update status gagal. terjadi error saat update proses");die();
//                    break;
//                }
//            }
//        }else{
//            return $this->response_gagal("02", "Proses gagal, silahkan ulangi kembali");die();
//        }
        
//        $collection = "uraian_".$data_pekerjaan_sub_id."_".$data_pekerjaan_uraian_id;
//        $key = "detail_".$data_kegiatan_id."_".$data_pekerjaan_id."_".$data_pekerjaan_sub_id."_".$data_pekerjaan_uraian_id."_".$data_pekerjaan_uraian_detail_id;
//        
//        $arr_insert = array(
//            "collection" => $collection,
//            "data" => array(
//                "key" => $key,
//                "keterangan" => $data_keterangan,
//                "aktifitas" => array(
//                    "tanggal" => date('Y-m-d'),
//                    "proses" => $data_progress,
//                    "gambar" => $data_image,
//                )
//            )
//        );
//        
//        $response_insert = json_decode($this->sendPostData($arr_insert, "insert-aktifitas"));
//        if(!empty($response_insert) && $response_insert->rc == "00"){
//            return $this->response_sukses("Proses berhasil");
//        }else{
//            return $this->response_gagal("02", "Proses gagal, silahkan ulangi kembali");
//        }
        
    }
    
    public function aktifitas_detail_verifikasi($param){
        if(empty($param->param->id)){
            return $this->response_gagal("02", "ID pekerjaan tidak tersedia");die();
        }
        $id = $param->param->id;
        $query = "SELECT l.id, l.pekerjaan_uraian_id, l.collection_mongo,l.key_mongo,l.ket_mongo,
            l.tanggal, l.gambar, l.proses, l.status, l.longitude, l.latitude, l.created_at, l.tanggal_approve,
            p.pekerjaan_nama, 
            ps.pekerjaan_sub_id, ps.pekerjaan_sub_nama,
            pu.pekerjaan_uraian_id, pu.pekerjaan_uraian_nama
            FROM laporan_aktifitas l
            left join pekerjaan_uraian pu on l.pekerjaan_uraian_id = pu.pekerjaan_uraian_id
            left join pekerjaan_sub ps on pu.pekerjaan_sub_id = ps.pekerjaan_sub_id
            left join pekerjaan p on ps.pekerjaan_id = p.pekerjaan_id
            where p.pekerjaan_id = 1
            and l.status = 0
            order by pu.pekerjaan_uraian_nama asc";
        $result = $this->db_prod->query($query, $id);
        if($result->num_rows() > 0){
            $no = 1;
            $data_pekerjaan = array();
            foreach($result->result() as $row){
                $data_pekerjaan[] = array(
                    "id" => $row->id,
                    "pekerjaan_uraian_id" => $row->pekerjaan_uraian_id,
                    "collection_mongo" => $row->collection_mongo,
                    "key_mongo" => $row->key_mongo,
                    "ket_mongo" => $row->ket_mongo,
                    "tanggal" => $row->tanggal,
                    "gambar" => $row->gambar,
                    "proses" => $row->proses,
                    "status" => $row->status,
                    "longitude" => $row->longitude,
                    "latitude" => $row->latitude,
                    "created_at" => $row->created_at,
                    "tanggal_approve" => $row->tanggal_approve,
                    "pekerjaan_nama" => $row->pekerjaan_nama,
                    "pekerjaan_sub_id" => $row->pekerjaan_sub_id,
                    "pekerjaan_sub_nama" => $row->pekerjaan_sub_nama,
                    "pekerjaan_uraian_id" => $row->pekerjaan_uraian_id,
                    "pekerjaan_uraian_nama" => $row->pekerjaan_uraian_nama
                );
            }
            
            // create array data
            $arr_group = $this->group_by("pekerjaan_uraian_nama", $data_pekerjaan);
            $array_group_data = array();
            foreach($arr_group as $row_group => $value){
                $array_group_detail_data = array();
                $arr_group_detail = $this->group_by("ket_mongo", $value);
                foreach($arr_group_detail as $row_group_detail => $value_detail){
                    $array_group_detail_data[] = array(
                        "detail_pekerjaan" => $row_group_detail,
                        "rincian" => $value_detail
                    );
                }
                $array_group_data[] = array(
                    "pekerjaan" => $row_group,
                    "detail" => $array_group_detail_data
                );
            }
            $no = 1;
            foreach(json_decode(json_encode($array_group_data)) as $r){
                $html .= "<div class='data-verifikasi'>";
                $html .= "<div class='col-md-12 col-xs-12'>
                            <p class='title-pekerjaan'><b>".$no.". ".ucwords(strtolower($r->pekerjaan))."</b></p>
                        </div>";
                
                foreach($r->detail as $r_detail){
                    $html .= "<div class='col-md-12 col-xs-12' style='margin-bottom:3px;'>
                            <p class='title-pekerjaan'><b><i class='fa fa-1x fa-angle-double-right'></i>&nbsp;&nbsp;".ucwords(strtolower($r_detail->detail_pekerjaan))."</b></p>
                        </div>";
                    
                    foreach($r_detail->rincian as $rincian){
                        $html .= "<div class='col-md-12 col-xs-12'>
                            <div class='box box-default box-solid' style='box-shadow:0px;'>
                                <div class='box-body'>
                                    <img src='".$rincian->gambar."' class='img-responsive' style='height:auto; width:100%;'>
                                    
                                    <button type='button' style='color: #333; margin-top:10px;' class='btn btn-default btn-sm' onclick='angular.element(this).scope().delete_aktifitas(".$rincian->id.")'>
                                        <i class='fa fa-1x fa-trash'></i>
                                        <i style='font-style: normal; font-size: 12px;'>Batalkan</i>
                                    </button>
                                    <button type='button' style='color: #fff; margin-top:10px;' class='pull-right btn btn-primary btn-sm'  onclick='angular.element(this).scope().simpan_aktifitas(".$rincian->id.")'>
                                        <i class='fa fa-1x fa-save'></i>
                                        &nbsp;
                                        <i class='pull-right' style='font-style: normal; font-size: 12px;'>Verifikasi</i>
                                    </button>    
                                </div>
                            </div>
                        </div>";
                    }
                }
                $html .= "</div>";
                $no++;
            }
            return $this->response_sukses("", $html);
        }else{
            return $this->response_gagal_html("02", "Belum ada laporan");die();
        }
    }
    
    protected function group_by($key, $array) {
        $result = array();
        foreach($array as $val) {
            if(array_key_exists($key, $val)){
                $result[$val[$key]][] = $val;
            }else{
                $result[""][] = $val;
            }
        }
        return $result;
    }
    
    public function aktifitas_detail_verifikasi_action($param){
        if(empty($param->param->id)){
            return $this->response_gagal("02", "ID tidak tersedia");die();
        }
        if(empty($param->param->type)){
            return $this->response_gagal("02", "Tipe tidak tersedia");die();
        }
        
        $id = $param->param->id;
        $type = $param->param->type;
        
        $query = "SELECT * FROM laporan_aktifitas WHERE id = ?";
        $result = $this->db_prod->query($query, $id);
        if($result->num_rows() > 0){
            $row = $result->row();
            
            if(strtolower($type) == "delete"){
                // updata status to -1
                $update = array(
                    "status" => "-1",
                );
                $this->processing_update_data("laporan_aktifitas", "id", $id, $update);
                return $this->response_sukses("Proses berhasil");
            }else{
                $arr_insert = array(
                    "collection" => $row->collection_mongo,
                    "data" => array(
                        "key" => $row->key_mongo,
                        "keterangan" => $row->ket_mongo,
                        "aktifitas" => array(
                            "tanggal" => $row->tanggal,
                            "proses" => $row->proses,
                            "gambar" => $row->gambar,
                            "longitude" => $row->longitude,
                            "latitude" => $row->latitude
                        )
                    )
                );
                $response_insert = json_decode($this->sendPostData($arr_insert, "insert-aktifitas"));
                if(!empty($response_insert) && $response_insert->rc == "00"){
                    // updata status 1
                    $update = array(
                        "status" => "1",
                        "tanggal_approve" => date("Y-m-d H:i:s"),
                    );
                    $this->processing_update_data("laporan_aktifitas", "id", $id, $update);
                    return $this->response_sukses("Proses berhasil");
                }else{
                    return $this->response_gagal("02", "Proses gagal, silahkan ulangi kembali");
                }
            }
        }else{
            return $this->response_gagal_html("02", "");die();
        }
    }
    
    protected function sendPostData($param = "", $method) {
        $url = $this->api_url."/".$method;
        $ch = curl_init();
        
        $param_data = json_encode($param);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 500);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param_data);
        
        $response = curl_exec($ch);  
        curl_close($ch); 
        return $response;
    }
}