<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Inbox model
============================================================== */
class Inbox extends MY_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function inbox_data($param){
        if(empty($param->param->kontraktor_id)){
            return $this->response_gagal("02", "Kontraktod ID tidak tersedia");die();
        }
        $id = $param->param->kontraktor_id;
        $limit = $param->param->limit;
        $query = "SELECT * FROM notification WHERE tipe = 'firebase' AND stats in ('sent','read') AND userid = ?";
        if($limit != ""){
            $query = "SELECT * FROM notification WHERE tipe = 'firebase' AND stats in ('sent','read') AND userid = ? order by id desc LIMIT $limit";
        }
        
        $result = $this->db_prod->query($query, $id);
        if($result->num_rows() > 0){
            $no = 0;
            $html = "";
            foreach($result->result() as $row){
                $tgl_dibuat = $this->format_date_indo($row->created_time);
                if($tgl_dibuat == "01 Januari 1970"){
                    $tgl_dibuat = "";
                }
                 
                $tgl_kirim = $this->format_date_indo($row->sent_time);
                if($tgl_kirim == "01 Januari 1970"){
                    $tgl_kirim = "";
                }
                
                $tgl_dibaca = $this->format_date_indo($row->read_time);
                if($tgl_dibaca == "01 Januari 1970"){
                    $tgl_dibaca = "";
                }
                
                $status = $row->stats;
                if($status == 'sent'){
                    $html .= "<div class='col-md-6' style='padding: 0px;'>
                        <a href='#/inbox_detail/".$row->id."'>
                            <div class='box-footer box-comments' style='background-color: #fff;'>
                              <div class='box-comment'>
                                <img class='img-circle' src='assets/img/cs-icon.png' alt='User Image'>
                                <div class='comment-text'>
                                    <span class='username' style='color:#000;'>
                                        ". (ucwords(strtolower($row->title)))."
                                        <span class='text-muted pull-right'>".$tgl_kirim."</span>
                                    </span>
                                    <span class=text-deskripsi-chat><b>". strip_tags(str_replace(array("<p>","</p>","<b>","</b>"), "", $row->content))."<b></span>
                                </div>
                              </div>
                            </div>
                        </a>    
                    </div>";
                    
                }else{
                    $html .= "<div class='col-md-6' style='padding: 0px; margin-bottom:-5px;'>
                        <a href='#/inbox_detail/".$row->id."'>
                            <div class='box-footer box-comments' style='background-color: #fff;'>
                              <div class='box-comment'>
                                <img class='img-circle' src='assets/img/cs-icon.png' alt='User Image'>
                                <div class='comment-text'>
                                    <span class='username' style='color:#000; font-weight:normal'>
                                        ". (ucwords(strtolower($row->title)))."
                                    <span class='text-muted pull-right' style='color:#333;'>".$tgl_kirim."</span>
                                    </span>
                                    <span class=text-deskripsi-chat>".strip_tags(str_replace(array("<p>","</p>","<b>","</b>"), "", $row->content))."</span>
                                </div>
                              </div>
                            </div>
                        </a>    
                    </div>";
                }
                
                    
                
                $no++;
            }
            return $this->response_sukses("", $html);
        }else{
            return $this->response_gagal_html("02", "Tidak ada berita terbaru");die();
        }
    }
    
    public function inbox_detail($param)
    {
        if(empty($param->param->id)){
            return $this->response_gagal("02", "ID inbox tidak tersedia");die();
        }
        
        $query = "SELECT * FROM notification where id = ?";
        $result = $this->db_prod->query($query, $param->param->id);
        if($result->num_rows() > 0){
            
            // update status to read
            $updatedata = array(
                "stats" => "read",
                "read_time" => date('Y-m-d H:i:s')
            );
            $this->processing_update_data("notification", "id", $param->param->id, $updatedata);
            
            $row = $result->row();
            $tgl_update = $this->format_date_indo($row->sent_time);
            if($tgl_update == "01 Januari 1970"){
                $tgl_update = "";
            }
            
            $resp = array(
                "id" => $row->id,
                "judul" => ucwords(strtolower($row->title)),
                "deskripsi" => strip_tags(str_replace(array("<p>","</p>","<b>","</b>"), "", $row->content)),
                "tanggal_dibuat" => $this->format_date_indo($row->created_time),
                "tanggal_update" => $tgl_update,
            );
            return $this->response_sukses($resp);
        }else{
            return $this->response_gagal_html("02", "Inbox tidak ditemukan");
        }
    }
}