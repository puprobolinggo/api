<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Keuangan model
============================================================== */
class Keuangan extends MY_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function laporan_keuangan($param){
        if(empty($param->param->kontraktor_id)){
            return $this->response_gagal("02", "Kontraktod ID tidak tersedia");die();
        }
        $id = $param->param->kontraktor_id;
        $limit = $param->param->limit;
        $query = "SELECT * FROM pekerjaan where kontraktor_id = ?";
        if($limit != ""){
            $query = "SELECT * FROM pekerjaan where kontraktor_id = ? order by pekerjaan_id desc LIMIT $limit";
        }
        
        $result = $this->db_prod->query($query, $id);
        if($result->num_rows() > 0){
            $no = 0;
            foreach($result->result() as $row){
                $tgl_kontrak = $this->format_date_indo($row->kontrak_tanggal);
                if($tgl_kontrak == "01 Januari 1970"){
                    $tgl_kontrak = "";
                }
                 
                $tgl_mulai = $this->format_date_indo($row->kontrak_mulai);
                if($tgl_mulai == "01 Januari 1970"){
                    $tgl_mulai = "";
                }
                
                $tgl_selesai = $this->format_date_indo($row->kontrak_selesai);
                if($tgl_selesai == "01 Januari 1970"){
                    $tgl_selesai = "";
                }
                $resp[] = array(
                    "pekerjaan_id" => $row->pekerjaan_id,
                    "pekerjaan_nama" => $row->pekerjaan_nama,
                    "kontrak_no" => $row->kontrak_no,
                    "kontrak_tanggal" => $tgl_kontrak,
                    "kontrak_mulai" => $tgl_mulai,
                    "kontrak_selesai" => $tgl_selesai,
                    "kontrak_nilai" => $row->kontrak_nilai,
                    "asisten" => $row->asisten,
                    "asisten_nip" => $row->asisten_nip,
                    "status" => $row->status,
                );
                $no++;
            }
            
            $response = array(
                "jumlah" => $no,
                "detail" => $resp,
            );
            
            // create respon html
            $html = "";
            foreach($response['detail'] as $row){
                $status = $row['status'];
                $icon = "box-success";
                if(strtolower($status) == 'belum dimulai'){
                    $icon = "box-default";
                }else if(strtolower($status) == 'dalam pengerjaan'){
                    $icon = "box-default";
                }else if(strtolower($status) == 'selesai'){
                    $icon = "box-default";
                }else{
                    $icon = "box-default";
                }
                
                $html .= "<div class='col-md-12 col-xs-12'>
                                <div class='box ".$icon." box-solid'>
                                    <div class='box-header with-border'>
                                        <a href='#/laporan_keuangan_detail/".$row['pekerjaan_id']."'' style='color: #333;'>
                                        
                                        <i style='font-style: normal; font-size: 14px;margin-right:10px; font-weight:bold;'>".ucwords(strtolower($status))."</i>
                                            <span class='fa fa-1x fa-angle-double-right  pull-right' style='margin-top:2px; margin-left:5px;'></span>
                                        <i class='pull-right' style='font-style: normal; font-size: 12px;'>Lihat Rincian Laporan</i>
                                        </a>    
                                    </div>
                                    <div class='box-body'>
                                    <a href='#/laporan_keuangan_detail/".$row['pekerjaan_id']."'' style='color: #333;'>
                                        <span class='fa fa-1x fa-calendar-plus-o'></span>
                                        Waktu Pengerjaan 
                                        (<i style='font-style: normal; font-size: 14px'>$tgl_mulai</i>
                                        -
                                        <i style='font-style: normal; font-size: 14px'>$tgl_selesai</i>)
                                        <br>
                                        <p class='title-pekerjaan' style='margin-top:5px;'><b>".ucwords(strtolower($row['pekerjaan_nama']))."</b></p>
                                        <p class='title-pekerjaan'>Nilai Kontrak : ".  number_format($row['kontrak_nilai'], 0 , ',', '.')."</p>
                                    </a>
                                    </div>
                                </div>
                            </div>";
            }
            
            return $this->response_sukses($response, $html);
        }else{
            return $this->response_gagal_html("02", "Tidak ada berita terbaru");die();
        }
    }
    
    public function laporan_keuangan_detail($param){
        if(empty($param->param->id)){
            return $this->response_gagal("02", "ID pekerjaan tidak tersedia");die();
        }
        $id = $param->param->id;
        $query = "SELECT pu.pekerjaan_uraian_id, pu.pekerjaan_sub_id, pu.pekerjaan_uraian_nama,
            pu.pekerjaan_uraian_satuan, pu.pekerjaan_uraian_volume, pu.pekerjaan_uraian_volume_update,
            pu.pekerjaan_uraian_bobot,pu.pekerjaan_uraian_harga, pu.pekerjaan_uraian_total,
            pu.pekerjaan_uraian_keterangan
            FROM pekerjaan_uraian pu
            LEFT JOIN pekerjaan_sub ps on pu.pekerjaan_sub_id = ps.pekerjaan_sub_id
            left join pekerjaan p on ps.pekerjaan_id = p.pekerjaan_id
            where p.pekerjaan_id = ? order by pu.pekerjaan_uraian_id asc";
        $result = $this->db_prod->query($query, $id);
        if($result->num_rows() > 0){
            $no = 1;
            
            $total = 0;
            $html = "<div class='col-md-12 col-xs-12'>
                            <table class='table table-bordered'>";
            foreach($result->result() as $row){
                $html .= "
                                <tr>
                                    <td>".$no."</td>
                                    <td>".  ucwords(strtolower($row->pekerjaan_uraian_nama))."</td>
                                    <td style='text-align:right;'>".number_format($row->pekerjaan_uraian_total, 0 , ',', '.')."</td>
                                </tr>
                    ";
                $total += $row->pekerjaan_uraian_total;
                $no++;
            }
            $html .= "<tr><td colspan=2><b>Total</b></td><td><b>".number_format($total, 0 , ',', '.')."</b></td></tr>
                
                        </table>
                        </div>";
            
            return $this->response_sukses("", $html);
        }else{
            return $this->response_gagal_html("02", "Tidak ada laporan keuangan");die();
        }
    }
}