<?php defined('BASEPATH') OR exit('No direct script access allowed');
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : API controller
============================================================== */
class Api extends Site_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
    public function index(){
        $req = json_decode(file_get_contents('php://input'));
        if(empty($req)){
            echo $this->response_gagal("01", "Wrong parameter");die();
        }
        
        if(empty($req->method)){
            echo $this->response_gagal("01", "Wrong Method");die();
        }
        
        if(empty($req->processing_code)){
            echo $this->response_gagal("01", "Wrong Processing Code");die();
        }
        
        $this->load->library('akses');
        $method = $req->method;
        $proc_code = $req->processing_code;
        $modelname = $this->akses->get_akses($method);
        if($modelname == ""){
            echo $this->response_gagal("01", "Unknown Method");die();
        }
        
        $parseModel = explode("#", $modelname);
        $nama_model = $parseModel[0];
        $path_model = $parseModel[1];
        $route_model = $parseModel[2];
        
        try{
            $this->load->model($path_model);
        } catch (Exception $ex) {
            echo $this->response_gagal("01", "Model App not exist");die();
        }
        $result = $this->$nama_model->$route_model($req);
        echo $result;
    }
    public function app(){
        $this->load->helper('download');
        $act_download = $this->input->post("btn_download", true);
        $act_panduan = $this->input->post("btn_panduan", true);
        
        if($act_download){
            // read file contents
            $filename = "silakon.apk"; 
            $data = file_get_contents(base_url('/assets/'.$filename));
            force_download($filename, $data);
            
        }
        
        if($act_panduan){
            $filename = "panduan.pdf"; 
            $data = file_get_contents(base_url('/assets/'.$filename));
            force_download($filename, $data);
        }
        
        $this->load->view("app");
    }
    
    
}