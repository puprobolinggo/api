<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class 
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Core Model
============================================================== */
MY_Model extends CI_Model {

    public $db_prod;
    public $db_devel;
    
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    
        $this->db_prod = $this->load->database('default', TRUE);
        $this->db_devel = $this->load->database('devel', TRUE);
    }
    
    
    public function response_sukses($param, $detail = ""){
        $data = array(
            "status" => "00",
            "description" => "SUCCESS",
            "data" => $param,
            "detail" => $detail
        );
        return json_encode($data);
    }
    
    public function response_gagal($rc, $ket, $resp = ""){
        $data = array(
            "status" => $rc,
            "description" => $ket,
            "data" => $resp
        );
        return json_encode($data);
    }
    
    public function response_gagal_html($rc, $ket, $resp = ""){
        $html = "<div class='col-md-12 col-xs-12' style='margin-top:150px;'>
                    <center>
                        <img src='assets/img/search-icon.png' class='img-responsive'>
                    </center>
                </div><div class='col-md-12 col-xs-12'>
                    <center>
                        <b>Maaf!!!data tidak tersedia</b>
                    </center>
                </div>";
        $data = array(
            "status" => $rc,
            "description" => $html,
            "data" => $resp
        );
        return json_encode($data);
    }
    
    public function processing_insert_data($table, $data_insert){
        $this->db_prod->insert($table, $data_insert);
        return $this->db_prod->insert_id();
    }
    
    public function processing_update_data($table, $key_name, $key_id, $data_update){
        try{
            $this->db_prod->where($key_name, $key_id);
            $this->db_prod->update($table, $data_update);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    public function processing_delete_data($table, $key_name, $key_id){
        try{
            $this->db_prod->where($key_name, $key_id);
            $this->db_prod->delete($table);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    public function format_date_indo($tanggal){
	$date = date('Y-m-d',strtotime($tanggal));
        if($date == '0000-00-00'){
            return '';
        }
            

        $tgl = substr($date, 8, 2);
        $bln = substr($date, 5, 2);
        $thn = substr($date, 0, 4);

        switch ($bln) {
            case 1 : {
                    $bln = 'Jan';
                }break;
            case 2 : {
                    $bln = 'Feb';
                }break;
            case 3 : {
                    $bln = 'Maret';
                }break;
            case 4 : {
                    $bln = 'April';
                }break;
            case 5 : {
                    $bln = 'Mei';
                }break;
            case 6 : {
                    $bln = "Juni";
                }break;
            case 7 : {
                    $bln = 'Juli';
                }break;
            case 8 : {
                    $bln = 'Agustus';
                }break;
            case 9 : {
                    $bln = 'Sept';
                }break;
            case 10 : {
                    $bln = 'Okt';
                }break;
            case 11 : {
                    $bln = 'Nov';
                }break;
            case 12 : {
                    $bln = 'Des';
                }break;
            default: {
                    $bln = 'UnKnown';
                }break;
        }

        $hari = date('N', strtotime($date));
        switch ($hari) {
            case 0 : {
                    $hari = 'Minggu';
                }break;
            case 1 : {
                    $hari = 'Senin';
                }break;
            case 2 : {
                    $hari = 'Selasa';
                }break;
            case 3 : {
                    $hari = 'Rabu';
                }break;
            case 4 : {
                    $hari = 'Kamis';
                }break;
            case 5 : {
                    $hari = "Jum'at";
                }break;
            case 6 : {
                    $hari = 'Sabtu';
                }break;
            default: {
                    $hari = 'UnKnown';
                }break;
        }

//        $tanggalIndonesia = "Hari ".$hari.", Tanggal ".$tgl . " " . $bln . " " . $thn;
        $tanggalIndonesia = $tgl . " " . $bln . " " . $thn;
        return $tanggalIndonesia;
    
    }
}
?>