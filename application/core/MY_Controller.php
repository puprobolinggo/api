<?php
/* ===========================================================
::  => Author       : Robby Adnan F.
    => Email        : bobbyadnan17@gmail.com 
    => Description  : Site_Controller as CI Controller
============================================================== */
class Site_Controller extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        
        header('Access-Control-Allow-Origin: *'); 
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
        
        $this->data = array();
    }
    
    public function response_sukses($param, $detail = ""){
        $data = array(
            "status" => "00",
            "description" => "SUCCESS",
            "data" => $param,
            "detail" => $detail
        );
        return json_encode($data);
    }
    
    public function response_gagal($rc, $ket, $resp = ""){
        $data = array(
            "status" => $rc,
            "description" => $ket,
            "data" => $resp
        );
        return json_encode($data);
    }
}